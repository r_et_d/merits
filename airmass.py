from __future__ import print_function
import numpy as np
from matplotlib import rc_file
rc_file('pgf.rc')  # noqa
import matplotlib.pylab as plt

from figsize import figsize


#Veto function
def airmass_limit(airmass_threshold, model='secz'):
    ##Zenith angle:
    ## the angle between a direction of interest and the local zenith
    ##Local zenith:
    ## refers to an imaginary point directly "above" a particular location
    step = 0.1
    zenith_angle = np.arange(step, 90.+step, step)  # deg

    if model == 'pickering':
        # Pickering (2002)
        airmass = 1./libmerits.Astronomy().airmass(zenith_angle, model=model)
        zenith_angle = zenith_angle[np.argmin(np.abs(airmass-airmass_threshold))]
    else:  # default is sec z approximation
        # X = sec z (inverse easy to calculate)
        zenith_angle = np.rad2deg(np.arccos(1./airmass_threshold))

    airmass_elev = 90.-zenith_angle
    return airmass_elev  # deg


if __name__ == '__main__':

    import sys
    import libmerits

    if len(sys.argv) < 2:
        sys.argv.append('all')

# Hard limit
    if sys.argv[1] == 'veto' or sys.argv[1] == 'all':
        airmass_threshold = 10.
        airmass_elev = airmass_limit(airmass_threshold)
        print('elevation angle at airmass limit %s = %s [deg]'
              % (str(airmass_threshold), str(airmass_elev)))
        model = 'pickering'
        airmass_value = 1./libmerits.Astronomy().airmass(90.-airmass_elev,
                                                         model=model)
        text = (' X = %s = %.1f @ elev angle = %.2f deg'
                % (model, airmass_value, airmass_elev))
        print(text)
        model = 'secz'
        airmass_value = 1./libmerits.Astronomy().airmass(90.-airmass_elev)
        text = (' X = %s = %.1f @ elev angle = %.2f deg'
                % (model, airmass_value, airmass_elev))
        print(text)

    step = 1.
    elevation = np.arange(step, 90., step)  # deg

#Airmass curve
    if sys.argv[1] == 'curve' or sys.argv[1] == 'all':
        fig = plt.figure(facecolor='white')
        ax = plt.subplot(111)
        plt.hold(True)
        model = 'pickering'
        airmass_merit = libmerits.Astronomy().airmass(90.-elevation,
                                                      model=model)
        plt.plot(elevation, 1./airmass_merit,
                 'b--',
                 label=r'$%s$' % (model))
        model = 'secz'
        airmass_merit = libmerits.Astronomy().airmass(90.-elevation,
                                                      model=model)
        plt.plot(elevation, 1./airmass_merit,
                 'r:',
                 label=r'$%s$' % (model))
        plt.hold(False)
        plt.xlabel('Elevation Angle [Deg]')
        plt.ylabel(r'Airmass')
        plt.legend(loc=0, frameon=False)
        plt.savefig('airmass-curve.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)

#Airmass merit
    if sys.argv[1] == 'merit' or sys.argv[1] == 'all':
        fig = plt.figure(facecolor='white', figsize=figsize(0.95),
                         constrained_layout=True)
        ax = plt.subplot(111)
        plt.hold(True)
        model = 'pickering'
        lnstl = ['--', '-.']
        for idx, steepness in enumerate([1, 2]):
        # for idx, steepness in enumerate([1]):
            airmass_merit = libmerits.Astronomy().airmass(90.-elevation,
                                                          model=model,
                                                          steepness=steepness)
            plt.plot(elevation, airmass_merit,
                     linestyle=lnstl[idx], linewidth=1,
                     label=r'$%s\ \alpha = %d$' % (model, steepness))
        model = 'secz'
        default_steepness = 1.
        airmass_merit = libmerits.Astronomy().airmass(90.-elevation,
                                                      model=model,
                                                      steepness=default_steepness)
        plt.plot(elevation, airmass_merit,
                 'k:', linewidth=1,
                 label=r'$\sec(z_h)\ \alpha = %d\ (\mathrm{default})$' % (default_steepness))
        lnstl = [(0, (4, 2, 1, 2, 1, 2))]
        for idx, steepness in enumerate([3]):
            airmass_merit = libmerits.Astronomy().airmass(90.-elevation,
                                                          model=model,
                                                          steepness=steepness)
            plt.plot(elevation, airmass_merit,
                     linestyle=lnstl[idx], linewidth=1,
                     label=r'$\sec(z_h)\ \alpha = %d$' % (steepness))
        plt.hold(False)
        plt.title(r'$m=1/z_h^\alpha$')
        plt.ylabel('Airmass merit')
        plt.xlabel('Altitude [Deg]')
        leg = plt.legend(loc=0, fancybox=True)
        # set the alpha value of the legend: it will be translucent
        leg.get_frame().set_alpha(0.9)
        leg.get_frame().set_edgecolor('none')
        plt.savefig('airmass-merit.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('airmass-merit.pgf')

    plt.show()

# -fin-
