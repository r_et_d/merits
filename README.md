## Example implementations of selected merits

    python fairness.py fairness
    python fairness.py completion
    python fairness.py priority

    python airmass.py veto
    python airmass.py curve
    python airmass.py merit

    python deadtime.py curve
    python deadtime.py merit

    python separation.py merit
    python separation.py veto

    python referenceposition.py centroid
    python referenceposition.py earliest
    python referenceposition.py latest

    python completion.py
    python elevation.py

    python windows.py rise
    python windows.py set
    python windows.py window
    python windows.py approach

    python cadence.py gaussian
    python cadence.py box
    python cadence.py sharp

## General implementation
### Display merit options and usage

    python libmerit.py

### Tests

    python libmerit.py test
    bash show_merits.sh
