from __future__ import print_function
import numpy as np
from matplotlib import rc_file
rc_file('pgf.rc')
import matplotlib.pylab as plt
import matplotlib.dates as mdates
from datetime import datetime, timedelta

from figsize import figsize


if __name__ == '__main__':

    import sys
    import libmerits

    if len(sys.argv) < 2:
        sys.argv.append('all')

    # observation frequency of every X hours
    dT = 12.  # hours
    dt = timedelta(hours=dT).total_seconds()  # hours
    # simulate observations over 3 days
    now = datetime.now()
    t0 = now
    T = np.arange(t0, t0+timedelta(hours=72.), timedelta(minutes=10)).astype(datetime)  # hours
    t = np.asarray([ts.total_seconds() for ts in (T-t0)])

    # coherence with periodic function
    if sys.argv[1] == 'gaussian' or sys.argv[1] == 'all':
        fig, ax = plt.subplots(figsize=figsize(scale=0.49),
                               nrows=1, ncols=1,
                               facecolor='white')
        fig.autofmt_xdate()

        sigma = 1.
        mt = libmerits.Range(t).coherence(dt, sigma=sigma)
        plt.plot(T, mt,
                 'k--',
                 label=r'$\sigma=%.1f\ (\mathrm{default})$' % sigma)

        sigma = 8.
        mt = libmerits.Range(t).coherence(dt, sigma=sigma)
        plt.plot(T, mt,
                 'k:',
                 label=r'$\sigma=%.1f$' % sigma)

        plt.ylim([-0.01, 1.01])
        xfmt = mdates.DateFormatter('%d-%m-%y %H:%M')
        ax.xaxis.set_major_formatter(xfmt)
        plt.gcf().autofmt_xdate()
        plt.xlabel('Observation time [hours]')
        plt.ylabel('Phase coherence merit')
        plt.title(r'\raisebox{1.3ex}{$m=\exp(-(\frac{2\pi t/T}{\sigma})^2)$}')
        # plt.legend(loc="lower right", frameon=True)
        leg = plt.legend(loc='lower right', fancybox=True)
        # set the alpha value of the legend: it will be translucent
        leg.get_frame().set_alpha(0.9)
        leg.get_frame().set_edgecolor('none')
        plt.savefig('phasecoherence.png',
                    dpi=fig.dpi,
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('phasecoherence.pgf')

    # box
    if sys.argv[1] == 'box' or sys.argv[1] == 'all':

        fig, ax = plt.subplots(figsize=figsize(scale=0.49),
                               nrows=1, ncols=1,
                               facecolor='white')
        fig.autofmt_xdate()

        fwhm = 0.2  # 20%
        mt = libmerits.Range(t).box(dt, fwhm=fwhm)
        plt.plot(T, mt,
                 'k-',
                 label=r'$\sigma=%.0f$%% $(\mathrm{default})$' % (fwhm*100))

        fwhm = 0.5  # 50%
        mt = libmerits.Range(t).box(dt, fwhm=fwhm)
        plt.plot(T, mt,
                 'k:',
                 label=r'$\sigma=%.0f$%%' % (fwhm*100))

        plt.ylim([-0.01, 1.01])
        xfmt = mdates.DateFormatter('%d-%m-%y %H:%M')
        ax.xaxis.set_major_formatter(xfmt)
        plt.gcf().autofmt_xdate()
        plt.title(r'$m=1,\ -\delta t \leq t \leq \delta t$')
        plt.xlabel('Observation time [hours]')
        plt.ylabel('Box periodic merit')
        # plt.legend(loc="lower right", frameon=True)
        leg = plt.legend(loc='lower right', fancybox=True)
        # set the alpha value of the legend: it will be translucent
        leg.get_frame().set_alpha(0.9)
        leg.get_frame().set_edgecolor('none')
        plt.savefig('boxperiodic.png',
                    dpi=fig.dpi,
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('boxperiodic.pgf')

    # coherence with sharp rise to time
    if sys.argv[1] == 'sharp' or sys.argv[1] == 'all':
        fig, ax = plt.subplots(figsize=figsize(scale=0.49),
                               nrows=1, ncols=1,
                               facecolor='white')
        # fig.autofmt_xdate()

        # factor = 5.
        # mt = libmerits.Range(t).spike(delta=dt, factor=factor)
        # plt.plot(T, mt,
        #          ':',
        #          label=r'$\alpha= %.1f$' % factor)
        # factor = 4.
        # mt = libmerits.Range(t).spike(delta=dt, factor=factor)
        # plt.plot(T, mt,
        #          ':',
        #          label=r'$\alpha= %.1f$' % factor)
        factor = 3.
        mt = libmerits.Range(t).spike(delta=dt, factor=factor)
        plt.plot(T, mt,
                 'k-',
                 label=r'$\alpha= %.1f\ (\mathrm{default})$' % factor)
        factor = 2.
        mt = libmerits.Range(t).spike(delta=dt, factor=factor)
        plt.plot(T, mt,
                 'k:',
                 label=r'$\alpha= %.1f$' % factor)
        plt.ylim([-0.01, 1.01])
        xfmt = mdates.DateFormatter('%d-%m-%y %H:%M')
        ax.xaxis.set_major_formatter(xfmt)
        plt.gcf().autofmt_xdate()
        plt.title(r'$m=a(\alpha)^{\delta t}$')
        plt.xlabel('Observation time [hours]')
        plt.ylabel('Sharp periodic merit')
        # plt.legend(loc="lower right", frameon=True)
        leg = plt.legend(loc='lower right', fancybox=True)
        # set the alpha value of the legend: it will be translucent
        leg.get_frame().set_alpha(0.9)
        leg.get_frame().set_edgecolor('none')
        plt.savefig('spikeperiodic.png',
                    dpi=fig.dpi,
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('spikeperiodic.pgf')

    plt.show()


# -fin-
