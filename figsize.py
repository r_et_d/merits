from __future__ import print_function

import numpy as np

textwidth = {
    'spie': 487.8225,
    'mphil': 412.56496,
    }


def figsize(scale=0.9, the_textwidth=textwidth['mphil'], aspect=None):
    r"""Calculate the LaTeX figure dimensions

    Args:
        scale (float): the image size relative to '\textwitdh' or '\linewidth'
        the_textwidth (float): is the value LaTeX gives for '\the\textwidth'
        aspect (float): the figure height relative to the width -- if
            unspecified defaults to the golden ratio, which produces a
            pleasing landscape figure; a value of 1.0 results in a square
            figure; values less than 1 produce landscape style figures and
            values greater than 1 produce portrait style

    Returns:
        fig_dims: a list containing the figure width and height, as
        floating-point inch values
    """
    inches_per_pt = 1.0 / 72.27  # as defined in TeX

    if aspect is None:
        golden_mean = (np.sqrt(5) - 1) / 2.0
        aspect = golden_mean

    fig_width_pt = the_textwidth * scale
    fig_width_in = fig_width_pt * inches_per_pt
    fig_height_in = fig_width_in * aspect
    fig_dims = [fig_width_in, fig_height_in]

    return fig_dims
