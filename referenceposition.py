from __future__ import print_function
from datetime import datetime, timedelta
import ephem
import numpy as np
from matplotlib import rc_file
rc_file('pgf.rc')
import matplotlib.pylab as plt
import matplotlib.dates as mdates

from figsize import figsize

# mean rise time - (default) centroid calculated coordinate
def centroid(observer, targets):
    eval_list = np.array([[body.ra.real, body.dec.real] for body in targets])
    ref_pos = ephem.FixedBody()
    ref_pos.name = 'reference'
    ref_pos._ra = np.median(eval_list[:, 0])
    ref_pos._dec = np.median(eval_list[:, 1])
    return ref_pos


# earliest rise time
def first(observer, targets):
    risetimes = [body.ra.real for body in targets]
    ref_pos = targets[np.argmin(risetimes)].copy()
    ref_pos.name = 'reference'
    return ref_pos


# latest rise time
def last(observer, targets):
    risetimes = [float(ephem.Date(observer.next_rising(body, ephem.now()))) for body in targets]
    ref_pos = targets[np.argmax(risetimes)].copy()
    ref_pos.name = 'reference'
    return ref_pos


# get reference position
def set_sky_ref(ref_position, targets):
    return ref_position(*targets)


def build_obs_list(observer, target_list, reference):
    targets = []
    for idx, target in enumerate(target_list):
        ra, dec = target
        # define your own celestial target as a fixedbody
        target = ephem.FixedBody()
        target.name = 'target'
        target._ra = ra
        target._dec = dec
        target.compute(observer)
        targets.append(target)
    # to evaluate you need a reference position for the observation
    # since this does not change, add it as a new target to the observation
    # target list
    targets.append(set_sky_ref(reference, (observer, targets)))
    return targets


if __name__ == '__main__':

    import sys
    import libmerits
    if len(sys.argv) < 2:
        sys.argv.append('default')

    # geocentric location from which the target will be observed
    observer = ephem.Observer()
    observer.lon = '21:24:38.5'
    observer.lat = '-30:43:17.3'
    observer.elevation = 1038.0
    observer.date = ephem.Date(datetime.now())

    # list of targets in the observation: target=(ra,dec)
    target_list = [['12:58:55.21', '27:42:33.2'],
                   ['14:43:57.35', '12:41:21.3'],
                   ['16:35:31.48', '-7:20:54.3'],
                   ]

    # you are going to use such a list for each observation,
    # but you only need to construct it once, when the proposal
    # has been accepted and the observations are added as projects
    if sys.argv[1] == 'centroid' or sys.argv[1] == 'default':
        targets = build_obs_list(observer, target_list, centroid)

    if sys.argv[1] == 'earliest':
        targets = build_obs_list(observer, target_list, first)

    if sys.argv[1] == 'latest':
        targets = build_obs_list(observer, target_list, last)

    now = ephem.now()
    observer.date = now
    for target in targets:
        target.compute(observer)
        print('\n', target.name)
        print('\t', target.ra, target.dec)
        print('\trise time', observer.next_rising(target, now))
        print('\ttransit time and altitude',
              observer.next_transit(target, now),
              np.rad2deg(float(target.transit_alt)))
    sky_ref_pos = targets[-1]
    rising = observer.next_rising(targets[0], now)
    setting = observer.next_setting(targets[-2], rising)
    track = np.arange(rising.datetime(),
                      setting.datetime(),
                      timedelta(minutes=10)).astype(datetime)

    fig = plt.figure(figsize=figsize(0.49, aspect=1/1.365978717419099), facecolor='white')
    ax = plt.subplot(111)
    mkrs = ['^', 'o', 'v']
    # for idx, target in enumerate(targets[:-1]):
    #     ax.scatter(np.rad2deg(target.ra.real), np.rad2deg(target.dec.real),
    #                50, marker=mkrs[idx], color='k', alpha=0.4,
    #                label='target%d' % (idx))
    # ax.scatter(np.rad2deg(sky_ref_pos.ra.real),
    #            np.rad2deg(sky_ref_pos.dec.real),
    #            150, marker='+', color='k',
    #            label='reference (%.2f, %.2f)' %
    #                  (np.degrees(sky_ref_pos.ra), np.degrees(sky_ref_pos.dec)))
    # plt.grid('on')
    # plt.legend(loc="lower left",
    #            markerscale=1.,
    #            scatterpoints=1,
    #            fontsize=12,
    #            frameon=False)
    # plt.ylabel('DEC [deg]', fontsize=16)
    # plt.xlabel('RA [deg]', fontsize=16)
    alpha = 1.
    transit_alt = targets[0].transit_alt
    altitude = []
    for t in track:
        observer.date = t
        targets[0].compute(observer)
        altitude.append(np.degrees(targets[0].alt))
    plt.plot(track, altitude,
             'k:')
    transit_alt = targets[1].transit_alt
    altitude = []
    for t in track:
        observer.date = t
        targets[1].compute(observer)
        altitude.append(np.degrees(targets[1].alt))
    plt.plot(track, altitude,
             'k:')
    transit_alt = targets[2].transit_alt
    altitude = []
    for t in track:
        observer.date = t
        targets[2].compute(observer)
        altitude.append(np.degrees(targets[2].alt))
    plt.plot(track, altitude,
             'k:')
    transit_alt = sky_ref_pos.transit_alt
    altitude = []
    for t in track:
        observer.date = t
        sky_ref_pos.compute(observer)
        altitude.append(np.degrees(sky_ref_pos.alt))
    plt.plot(track, altitude,
             'k--',
             label='reference (%.2f, %.2f)' %
                   (np.degrees(sky_ref_pos.ra), np.degrees(sky_ref_pos.dec)))
    plt.legend(loc="lower left",
               markerscale=1.,
               scatterpoints=1,
               frameon=False)
    plt.title('Example tracks')
    plt.ylabel('Elevation [deg]')
    xfmt = mdates.DateFormatter('%H:%M')
    ax.xaxis.set_major_formatter(xfmt)
    plt.gcf().autofmt_xdate()
    plt.xlabel(r'Time')

    altitude = []
    for t in track:
        observer.date = t
        sky_ref_pos.compute(observer)
        altitude.append(sky_ref_pos.alt)

    fig.savefig('skyrefpos-a.pgf')

    fig = plt.figure(figsize=figsize(0.49, aspect=1/1.365978717419099), facecolor='white')
    # ax = plt.subplot(122)
    ax = plt.subplot(111)
    alpha = 1.
    plt.plot(track, libmerits.Range(altitude).maximum(transit_alt, alpha=alpha),
             'k',
             label=r'$\alpha=%.1f\ (\mathrm{default})$' % alpha)
    alpha = 2.
    plt.plot(track, libmerits.Range(altitude).maximum(transit_alt, alpha=alpha),
             'k',
             linestyle='--',
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 3.
    plt.plot(track, libmerits.Range(altitude).maximum(transit_alt, alpha=alpha),
             'k',
             linestyle=':',
             label=r'$\alpha=%.1f$' % alpha)
    leg = plt.legend(loc='upper right', fancybox=True)
    # set the alpha value of the legend: it will be translucent
    leg.get_frame().set_alpha(0.9)
    leg.get_frame().set_edgecolor('none')

    xfmt = mdates.DateFormatter('%H:%M')
    ax.xaxis.set_major_formatter(xfmt)
    plt.gcf().autofmt_xdate()
    plt.title(r'$m=(h_{t}/h_{max})^\alpha$')
    plt.ylabel('Reference position culmination')
    plt.xlabel(r'Time')
    plt.savefig('culmination-%s.png' % sys.argv[1],
                facecolor=fig.get_facecolor(),
                transparent=True)
    plt.savefig('skyrefpos-b.pgf')

    plt.show()

# -fin-
