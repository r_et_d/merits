from __future__ import print_function
import numpy as np
from matplotlib import rc_file
rc_file('pgf.rc')  # noqa
import matplotlib.pylab as plt

from figsize import figsize


if __name__ == '__main__':
    import sys
    import libmerits

    if len(sys.argv) < 2:
        sys.argv.append('all')

    # Variable that rise within a specified time
    # start merit -- favours target as it nears rise time
    if sys.argv[1] == 'rise' or sys.argv[1] == 'all':
        fig = plt.figure(facecolor='white', figsize=figsize(0.49))
        alpha = 1.
        deltat = 60.
        x_range = np.arange(-1., 1.001, .001)
        merit = []
        for dt in x_range:
            merit.append(libmerits.Increase().xboundary(alpha=alpha,
                                                        deltat=deltat*dt))
        plt.plot(deltat*x_range, merit,
                 'k-',
                 label=r'$\alpha=%.1f\ (default)$' % alpha)
        alpha = 3.
        merit = libmerits.Increase(deltat*x_range).xboundary(alpha=alpha)
        plt.plot(deltat*x_range, merit,
                 'k--',
                 label=r'$\alpha=%.1f$' % alpha)
        alpha = 10.
        merit = libmerits.Increase(deltat*x_range).xboundary(alpha=alpha)
        plt.plot(deltat*x_range, merit,
                 'k:',
                 label=r'$\alpha=%.1f$' % alpha)
        plt.axis('tight')
        plt.title(r'$m=\tanh((t_0-t)/\alpha)$')
        plt.legend(loc=0, frameon=False)
        plt.ylabel('Start merit')
        plt.xlabel(r'$t_0-t$')
        plt.savefig('rise-boundary.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('rise-boundary.pgf')

    # Variable that drops to zero within a specified time
    # end merit -- drops weight as the target nears set time
    if sys.argv[1] == 'set' or sys.argv[1] == 'all':
        brt = np.arange(-1., 1.001, .001)
        fig = plt.figure(facecolor='white', figsize=figsize(0.95),
                         constrained_layout=True)
        alpha = 1.
        deltat = 2.5  # 10.
        merit = libmerits.Decrease(deltat*brt).xboundary(alpha=alpha)
        plt.plot(deltat*brt, merit,
                 'k:',
                 label=r'$\alpha=%.1f\ (default)$' % alpha)
        alpha = 0.5
        merit = libmerits.Decrease(deltat*brt).xboundary(alpha=alpha)
        plt.plot(deltat*brt, merit,
                 '--',
                 label=r'$\alpha=%.1f$' % alpha)
        alpha = 0.1
        merit = libmerits.Decrease(deltat*brt).xboundary(alpha=alpha)
        plt.plot(deltat*brt, merit,
                 '-.',
                 label=r'$\alpha=%.1f$' % alpha)
        # plt.axis('tight')
        plt.title(r'$m=\tanh((t-t_0)/\alpha)$')
        plt.legend(loc=0, frameon=False)
        plt.ylabel('Stop merit')
        plt.savefig('set-boundary.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('set-boundary.pgf')

    # Variable that rises when the time remaining to observe this target in the current night drops.
    # window merit -- increases selection weight at the target observation window shortens
    if sys.argv[1] == 'window' or sys.argv[1] == 'all':
        step = 0.01
        tratio = np.arange(1, -step, -step)
        fig = plt.figure(facecolor='white', figsize=figsize(0.95),
                         constrained_layout=True)
        ax = plt.subplot(111)
        a = 1.; b = 1.; c = 0.
        plt.plot(tratio, libmerits.Increase(tratio).window(a, b, c),
                 'k:',
                 label=r'$a=%.1f,\,b=%.1f,\,c=%.1f\ (default)$' % (a, b, c))
        a = 0.25; b = 1.; c = 3.
        plt.plot(tratio, libmerits.Increase(tratio).window(a, b, c),
                 '--',
                 label=r'$a=%.1f,\,b=%.1f,\,c=%.1f$' % (a, b, c))
        a = 0.; b = 1.; c = 50.
        plt.plot(tratio, libmerits.Increase(tratio).window(a, b, c),
                 '-.',
                 label=r'$a=%.1f,\,b=%.1f,\,c=%.1f$' % (a, b, c))
        ax.invert_xaxis()
        plt.title(r'\raisebox{1.25ex}{$m = -a\times \Delta t + \left(\frac{b}{1+c\times \Delta t}\right)$}')
        plt.ylabel(r'Window merit')
        plt.xlabel(r'$\frac{t_{target}}{t_{night}}$')
        # plt.axis('tight')
        plt.legend(loc=0, frameon=False)
        plt.savefig('target-window-boundary.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('target-window-boundary.pgf')

    if sys.argv[1] == 'approach' or sys.argv[1] == 'all':
        fig = plt.figure(facecolor='white', figsize=figsize(0.49))
        alpha = 1.
        deltat = 60.
        x_range = np.arange(1.001, 0., -.001)
        merit = []
        for dt in x_range:
            merit.append(libmerits.Increase().approach(60.,
                                                       alpha=alpha,
                                                       deltat=deltat*dt))
        plt.plot(deltat*x_range, merit,
                 'k-',
                 label=r'$\alpha=%.1f\ (default)$' % alpha)
        alpha = 2.
        x_range = 60. * np.arange(1.001, 0., -.001)
        merit = libmerits.Increase(x_range).approach(60., alpha=alpha)
        plt.plot(x_range, merit,
                 'k--',
                 label=r'$\alpha=%.1f$' % alpha)
        alpha = 10.
        merit = libmerits.Increase(x_range).approach(60., alpha=alpha)
        plt.plot(x_range, merit,
                 'k:',
                 label=r'$\alpha=%.1f$' % alpha)
        plt.axis('tight')
        plt.title(r'$m=(t_0-t)/t_0^\alpha$')
        plt.legend(loc=0, frameon=False)
        plt.ylabel('Approach merit')
        plt.xlabel(r'$t$')
        plt.savefig('approach-window-boundary.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('approach-window-boundary.pgf')

    plt.show()

# -fin-
