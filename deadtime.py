from __future__ import print_function
import numpy as np
from matplotlib import rc_file
rc_file('pgf.rc')
import matplotlib.pylab as plt

from figsize import figsize


# some simple function to simulate slew curve
# if you do not want to use a constant speed
def slew_time(distance,  # angular distance deg
              rate,  # slew rate deg/sec
              ramptime=-1,  # time to achieve slew rate sec
              ):
    distance = np.asarray(distance)  # separation angle between where you are and first pointing of next observation
    if distance.size < 2:
        distance = np.asarray([distance])
    if ramptime < 0:
        # default assume constant speed
        time = distance/rate
    else:
        # distance to full rate
        dist = rate * ramptime
        accel = rate/ramptime
        time = np.empty(distance.shape)
        time[distance < dist] = 2.*np.square(distance[distance < dist]/accel)
        time[~(distance < dist)] = 2.*np.square(dist/accel) + \
            (distance[~(distance < dist)]-2.*dist)/rate
    return time


if __name__ == '__main__':

    import sys
    import libmerits

    if len(sys.argv) < 2:
        sys.argv.append('all')

    angle = np.arange(0, 30.1, 0.1)  # degrees
    creep_rate = 0.7  # deg/sec
    creep_time = angle/creep_rate

    # time allocation
    if sys.argv[1] == 'curve' or sys.argv[1] == 'all':
        delta_wait = 5.
        max_val = 25.
        creep_merit = libmerits.Decrease(creep_time).pwlinear(min_lim=delta_wait,
                                                              max_lim=max_val)
        curve_time = []
        merit = []
        for idx, dist in enumerate(angle):
            # assume motor reaches full rate of 5 deg/s after 10 sec
            curve_time.append(slew_time(dist, 4., 2.))
            merit_val = libmerits.Decrease(curve_time[-1]).pwlinear(min_lim=delta_wait,
                                                                    max_lim=max_val)
            merit.append(merit_val)

        next_wait = 20.
        next_max = 20.
        next_merit = libmerits.Decrease(curve_time).pwlinear(min_lim=next_wait,
                                                             max_lim=next_max)
        fig = plt.figure(figsize=(10, 12), facecolor='white')
        plt.subplot(311)
        plt.plot(angle, creep_time, 'm-.', label='constant')
        plt.plot(angle, curve_time, 'r--', label='slew')
        plt.title('Slew time curve')
        plt.ylabel('Time [sec]')
        plt.xlabel('Distance [deg]')
        plt.legend(loc=0, frameon=False)
        plt.subplot(312)
        plt.plot(creep_time, creep_merit,
                 'm-.',
                 label=r'$t_{wait}=%.1f\,[sec]\ t_{max}=%.1f\,[sec]$'
                       % (delta_wait, max_val))
        plt.plot(curve_time, merit,
                 'r--',
                 label=r'$t_{wait}=%.1f\,[sec]\ t_{max}=%.1f\,[sec]$'
                       % (delta_wait, max_val))
        plt.plot(curve_time, next_merit,
                 'b--',
                 label=r'$t_{wait}=%.1f\,[sec]\ t_{max}=%.1f\,[sec]$'
                       % (next_wait, next_max))
        plt.ylim([-0.01, 1.01])
        plt.xlabel('Time [sec]')
        plt.ylabel('Slew merit')
        plt.legend(loc=0, frameon=False)
        plt.subplot(313)
        ##Show various options for penalising dark time
        Cdead = libmerits.Decrease(np.asarray(creep_time)/max_val).inflection(alpha=1.)
        plt.plot(creep_time, Cdead,
                 'm-.',
                 label=r'$b=1.$')
        Cdead = libmerits.Decrease(np.asarray(curve_time)/max_val).inflection(alpha=0.5)
        plt.plot(curve_time, Cdead,
                 'r--',
                 label=r'$b=0.5$')
        Cdead = libmerits.Decrease(np.asarray(curve_time)/max_val).inflection(alpha=2.)
        plt.plot(curve_time, Cdead,
                 'b--', alpha=0.4)
        plt.plot(curve_time, libmerits.trim(curve_time, Cdead, max_value=max_val),
                 'b--',
                 label=r'$b=2:\ trim\ to\ max\ %d$' % max_val)
        plt.ylim([-0.01, 1.01])
        plt.xlabel('Time [sec]')
        plt.ylabel('Dead Time Penalty')
        plt.legend(loc=0)
        plt.savefig('slewcurve.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)

    if sys.argv[1] == 'merit' or sys.argv[1] == 'all':
        # assume motor reaches full rate of 4 deg/s after 2 sec
        time = slew_time(angle, 4., 2.)
        fig = plt.figure(figsize=figsize(0.49), facecolor='white')
        delta_wait = 5.
        max_val = 20.  # some 20% of the average observation period once that can be read from the observation database
        merit = libmerits.Decrease(time).pwlinear(min_lim=delta_wait, max_lim=max_val)
        plt.plot(time, merit,
                 'k:',
                 # label=r'$t_{wait}=%d\,[sec]\ t_{max}=%d\,[sec]$'
                 # % (delta_wait, max_val))
                 label='piecewise linear')
        # delta_wait = 10.
        # max_val = 10.
        # merit = libmerits.Decrease(time).pwlinear(min_lim=delta_wait, max_lim=max_val)
        # plt.plot(time, merit,
        #          'r:',
        #          label=r'$t_{wait}=%d\,[sec]\ t_{max}=%d\,[sec]$'
        #                % (delta_wait, max_val))
        merit = libmerits.Decrease(time).pwlinear()
        plt.plot(time, merit,
                 'k--',
                 label=r'linear')
        merit = libmerits.Instrument(time).slew(obstime=2)
        plt.plot(time, merit,
                 'k-',
                 label='ratio (default)')
        plt.ylim([-0.01, 1.01])
        plt.xlabel('Time [sec]')
        plt.ylabel('Slew merit')
        plt.title(r'\raisebox{1.3ex}{$m=\frac{t_0}{t_s + t_0}$}')
        plt.legend(loc=0, frameon=False)
        plt.savefig('slewmerit.png',
                    dpi=fig.dpi,
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('slewmerit.pgf')

    plt.show()

# -fin-
