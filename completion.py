from __future__ import print_function
import numpy as np
import matplotlib.pylab as plt


if __name__ == '__main__':

    import libmerits

# Similar to increase
# -- increase expect a normalised input
# -- maxout takes a list of values and a maximum value
    ##Show merits that rise and fall approaching a max value
    # compare to completion of fairness, the expects a normalised array as input
    Tgranted = 60.
    Ttotal = np.arange(0, Tgranted, 0.01)
    default_alpha = 1.
    Tcomplete = libmerits.Increase(Ttotal).tomax(lim_val=Tgranted, alpha=default_alpha)
    fig = plt.figure(facecolor='white')
    plt.plot(Ttotal, Tcomplete, 'k--',
             label=r'$\alpha=%.1f\ (default)$' % default_alpha)
    for alpha in [3., 10.]:
        merit = libmerits.Increase(Ttotal).tomax(lim_val=Tgranted, alpha=alpha)
        plt.plot(Ttotal, merit,
                 linestyle=':',
                 label=r'$\alpha=%.1f$' % alpha)
    plt.title(r'$w_2(\Delta)=(\Delta_{actual}/\Delta_{granted})^\alpha$')
    plt.ylabel('Completion factor')
    plt.xlabel(r'Time [sec]')
    plt.legend(loc=0, frameon=False)
    plt.savefig('timecompletion.png',
                facecolor=fig.get_facecolor(),
                transparent=True)

    # favour lateness
    Tremaining = libmerits.Decrease(Ttotal).tomin(lim_val=Tgranted, alpha=default_alpha)
    fig = plt.figure(facecolor='white')
    plt.plot(Ttotal, Tremaining, 'k--',
             label=r'$\alpha=%.1f\ (default)$' % default_alpha)
    for alpha in [3., 10.]:
        merit = libmerits.Decrease(Ttotal).tomin(lim_val=Tgranted, alpha=alpha)
        plt.plot(Ttotal, merit,
                 linestyle=':',
                 label=r'$\alpha=%.1f$' % alpha)
    plt.title(r'$m=1 - (T_{remaining}/T_{granted})^\alpha$')
    plt.ylabel('nocomplete factor')
    plt.xlabel(r'Time [sec]')
    plt.legend(loc=0, frameon=False)
    plt.savefig('westerntargets-merit.png',
                facecolor=fig.get_facecolor(),
                transparent=True)

    plt.show()

# -fin-
