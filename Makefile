clean:
	$(RM) *.pyc

clobber: clean
	$(RM) *.png
	$(RM) *.pdf
	$(RM) *.pgf

# -fin-
