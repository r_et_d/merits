from __future__ import print_function
import numpy as np
from matplotlib import rc_file
rc_file('pgf.rc')
import matplotlib.pylab as plt

from figsize import figsize


if __name__ == '__main__':

    import sys
    import libmerits

    if len(sys.argv) < 2:
        sys.argv.append('all')

    # time allocation
    if sys.argv[1] == 'fairness' or sys.argv[1] == 'all':
        default_alpha = 0.5
        # current time share remaining w2=(%granted-%used)
        time_share_remaining = np.arange(0, 1.001, 0.001)
        time_share_overspent = np.arange(-1, 0.001, 0.001)
# Range.inflection(%time_granted-%time_used)
# where the input value can be an array of values or a single evaluation value
        share = np.hstack([time_share_overspent, time_share_remaining])
        Tfairness = libmerits.Range(share).inflection(alpha=default_alpha)
        # show default shape of the merit
        # over the range of remain and overspent timeshares
        fig = plt.figure(facecolor='white', figsize=figsize(scale=0.49))
        plt.plot(share, Tfairness, 'k-',
                 label=r'$\alpha=%.1f\ (\mathrm{default})$' % default_alpha)
        # # show behaviour as you move to overtime
        # idx_range = slice(len(share)/2.-50,len(share)/2.+500, 10)
        # plt.plot(share[idx_range], Tfairness[idx_range], 'b.')

        # show merit shape options
        clrs = ['k--', 'k:']
        for idx, alpha in enumerate([1, 3]):
            Tfairness = libmerits.Range(share).inflection(alpha=alpha)
            plt.plot(share, Tfairness,
                     clrs[idx],
                     label=r'$\alpha=%.1f$' % alpha)
        plt.title(r'$\varepsilon(\%)=(\%_{granted}-\%_{actual})^\alpha$')
        plt.ylabel('Time fairness factor')
        plt.xlabel(r'$\%$')
        plt.legend(loc=0, frameon=False)
        plt.savefig('timefairness.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('timefairness.pgf')

    # favours completing projects to ensure publications
    if sys.argv[1] == 'completion' or sys.argv[1] == 'all':
        default_alpha = 0.5
        # fraction of total telescope time allocated to group/observation
        Tgranted = 60.  # seconds
        # Ttotal = total telescope observation time to date
        # Tactual = = Ttotal/Tgranted actual observation time fraction to date
        Ttotal = np.arange(0, Tgranted, 0.01)
        Tactual = Ttotal/Tgranted
        # evaluate (granted-actual) as %
        # show default shape of the merit
        # over the range of remain and overspent timeshares
# Range.inflection(fraction_time_observed)
# where the input value can be an array of values or a single evaluation value
        Tcomplete = libmerits.Increase(Tactual).inflection(alpha=default_alpha)
# or from completion.py
# Tcomplete = libmerits.Increase(Ttotal).tomax(max_val=Tgranted, alpha=default_alpha)
        fig = plt.figure(facecolor='white', figsize=figsize(scale=0.49))
        plt.plot(Tactual, Tcomplete, 'k-',
                 label=r'$\alpha=%.1f\ (\mathrm{default})$' % default_alpha)
        # for alpha in [0.2, 1, 3]:
        clrs = ['k--', 'k:']
        for idx, alpha in enumerate([1, 3]):
            Tcomplete = libmerits.Increase(Tactual).inflection(alpha=alpha)
            plt.plot(Tactual, Tcomplete,
                     clrs[idx],
                     label=r'$\alpha=%.1f$' % alpha)
        plt.title(r'$\varepsilon(\Delta)=(\Delta_{actual}/\Delta_{granted})^\alpha$')
        plt.ylabel('Completion factor')
        plt.xlabel(r'$\Delta$')
        plt.legend(loc=0, frameon=False)
        plt.savefig('timecompletion.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)
        plt.savefig('timecompletion.pgf')

    # scientific priority
    # single rank default behaviour
    # all observation have the project priority
    # or all observation individual priorities
    if sys.argv[1] == 'priority' or sys.argv[1] == 'all':
        n_obs = 5
        fig = plt.figure(facecolor='white')
        # Single priority for all observations/programs in the project
        priority = 2
        rank = libmerits.sPoly().flatrank(priority)
        plt.subplot(311)
        plt.plot(np.arange(n_obs), np.repeat(rank, n_obs),
                 'b*:',
                 label='Single project priority')
        plt.ylim([0, 1])
        plt.legend(loc=0)
        # Assigned normalised priority per observation/program
        plt.subplot(312)
        priority = [2, 1, 2, 3, 2]
        weights = [1, 0.1, 0.5, 0.75, 0.5]
        rank = libmerits.sPoly().flatrank(priority, weight=weights)
        plt.plot(np.arange(n_obs), rank,
                 'r*:',
                 label='Individual observation priorities')
        plt.ylim([0, 1])
        plt.legend(loc=0)
        plt.ylabel('Scientific Priority')
        # Mean normalised priority per observation/program
        plt.subplot(313)
        rank = libmerits.sPoly().flatrank(priority)
        plt.plot(np.arange(n_obs), rank, 'g*')
        rank = libmerits.sPoly().flatrank(priority, mean=True)
        plt.plot(np.arange(n_obs), np.repeat(rank, n_obs),
                 'g.:',
                 label='Mean priority')
        plt.xlabel(r'Observation [#]')
        plt.ylim([0, 1])
        plt.legend(loc=0)
        plt.savefig('sciencerank.png',
                    facecolor=fig.get_facecolor(),
                    transparent=True)

    plt.show()


# -fin-
