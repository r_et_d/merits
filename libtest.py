# flake8: noqa
import numpy
import matplotlib.pyplot as plt
from libmerits import *


# test phase coherency for periodic cadence
def phaseCoherence():

    merit = lambda t, dT, sigma: Range(t).coherence(dT, sigma=sigma)

    # observation frequency of every dT hours
    dT = 21.  # hours
    # observations over T days (in hours)
    T = 72.  # hours
    t = np.arange(0., T, 0.1)

    fig, ax = plt.subplots(figsize=(10, 5),
                           nrows=1, ncols=1,
                           facecolor='white')
    mt = np.cos(2.*np.pi*(t%dT)/dT)
    plt.plot(t, (1.+mt)/2., 'g:', label=r'$\cos$')

    for sigma in [1., 4., 10.]:
        plt.plot(t, merit(t, dT, sigma),
                ':',
                 label=r'$\exp \ \sigma=%.1f$' % sigma)

    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(0.9, 0.5))


# test boundary crossing rising curve over input ranges
def boundaryInflection():
    merit = lambda x_range, alpha: Range(x_range).inflection(alpha=alpha)
    nocenter = lambda x_range, alpha: Range(x_range).inflection(alpha=alpha,
                                                                center=False)

    def set_axis(ax):
        ax.set_ylim([-1.1, 1.1])
        ax.set_xlim([-1.1, 1.1])
        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(0.9, 0.5))

    alpha = 0.5
    fig, ax = plt.subplots(figsize=(12, 12),
                           nrows=2, ncols=1,
                           facecolor='white')
    # controlling ranges that goes over merit evaluation boundaries
    x_range = numpy.arange(-1.1, 1.1, 0.001)
    ext_merit = extend(x_range, nocenter(x_range, alpha),
                       min_value=-1, value=-1)
    ext_merit = extend(x_range, ext_merit,
                       max_value=1, value=1)
    ax[1].plot(x_range, ext_merit, 'y', label='set xrange boundaries')
    # standard merit
    x_range = numpy.arange(-1, 1.001, 0.001)
    ax[0].plot(x_range, merit(x_range, alpha),
               ':',
               label='standard merit')
    x_range = numpy.arange(-1., 0.001, 0.001)
    ax[1].plot(x_range, merit(x_range, alpha),
               linestyle='--',
               label='lower range')
    ax[0].plot(x_range, nocenter(x_range, alpha),
               '-.',
               label='decrease limit')
    x_range = numpy.arange(0., 1.001, 0.001)
    ax[1].plot(x_range, merit(x_range, alpha),
               linestyle='--',
               label='upper range')
    ax[0].plot(x_range, nocenter(x_range, alpha),
               '-.',
               label='increase limit')
    ax[0].set_title(r'$(-1\leq x \leq1)^\alpha$')
    ax[1].set_xlabel(r'$x$')
    set_axis(ax[0])
    set_axis(ax[1])


# test curve approaching some boundary over ranges and slopes
def boundaryApproach(decrease=False, increase=False):

    if decrease:
        merit = lambda x_range, alpha: Decrease(x_range).inflection(alpha=alpha)
    if increase:
        merit = lambda x_range, alpha: Increase(x_range).inflection(alpha=alpha)

    fig, ax = plt.subplots(figsize=(10, 5),
                           nrows=1, ncols=1,
                           facecolor='white')
    x_range = numpy.arange(0, 1.001, .001)
    alpha = 1.
    ax.plot(x_range, merit(x_range, alpha),
            linestyle='--',
            label=r'$\alpha=%.1f$' % alpha)
    x_range = numpy.arange(0, 5.001, .001)
    alpha = 0.5
    ax.plot(x_range, merit(x_range, alpha),
            linestyle='--',
            label=r'$\alpha=%.1f$' % alpha)
    x_range = numpy.arange(-1., 0.001, .001)
    alpha = 3
    ax.plot(x_range, merit(x_range, alpha),
            linestyle='--',
            label=r'$\alpha=%.1f$' % alpha)
    ax.set_title(r'Over X range: $x^\alpha$')
    ax.set_ylim([-0.03, 1.03])
    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(0.9, 0.5))


# test piecewise linear curves over input ranges and selections
def piecewiseLinear(rise=False, fall=False):
    if rise:
        merit = lambda x_range, wait, max_time: Increase(x_range).pwlinear(min_lim=wait, max_lim=max_time)
    if fall:
        merit = lambda x_range, wait, max_time: Decrease(x_range).pwlinear(min_lim=wait, max_lim=max_time)

    def plotmerit(ax, x_range, wait, max_time):
        ax.plot(x_range, merit(x_range, wait, max_time),
                linestyle='--',
                label=r'$t_{wait}=%.1f\,[sec]\ t_{max}=%.1f\,[sec]$'
                      % (wait, max_time))

    fig, ax = plt.subplots(figsize=(12, 12),
                           nrows=2, ncols=1,
                           facecolor='white')

    x_range = numpy.arange(0., 1., 0.001)
    wait = 0.1
    max_time = 0.7
    plotmerit(ax[0], x_range, wait, max_time),
    x_range = numpy.arange(-1., 1., 0.001)
    wait = -0.5
    max_time = 0.5
    plotmerit(ax[0], x_range, wait, max_time),
    wait = -0.4
    max_time = 0.7
    plotmerit(ax[0], x_range, wait, max_time),
    x_range = numpy.arange(-0.3, 0.3, 0.001)
    wait = -0.2
    max_time = 0.1
    plotmerit(ax[0], x_range, wait, max_time),
    ax[0].set_ylim([-0.01, 1.01])
    ax[0].set_title(r'$(\frac{t_{max}-t}{t_{max}-t_{wait}})$')
    # Shrink current axis by 20%
    box = ax[0].get_position()
    ax[0].set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    ax[0].legend(loc='center left', bbox_to_anchor=(0.9, 0.5))

    x_range = numpy.arange(1, 3, 0.001)
    wait = 1.4
    max_time = 2.7
    plotmerit(ax[1], x_range, wait, max_time),
    x_range = numpy.arange(0, 4, 0.001)
    wait = 1
    max_time = 3
    plotmerit(ax[1], x_range, wait, max_time),
    ax[1].set_ylim([-0.01, 1.01])
    ax[1].set_xlabel('Time [sec]')
    # Shrink current axis by 20%
    box = ax[1].get_position()
    ax[1].set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    ax[1].legend(loc='center left', bbox_to_anchor=(0.9, 0.5))


# crossing center over input ranges
def boundaryCross(down=False, rise=False):
    scale = 10.
    sigma = 1.
    if down:
        merit = lambda x_range, sigma, scale: Decrease(x_range).xboundary(sigma=sigma, scale=scale)
    if rise:
        merit = lambda x_range, sigma, scale: Increase(x_range).xboundary(sigma=sigma, scale=scale)

    fig, ax = plt.subplots(figsize=(8, 8),
                           nrows=1, ncols=1,
                           facecolor='white')
    x_range = numpy.arange(-1., 1.001, .001)
    ax.plot(x_range, merit(x_range, sigma, scale), '--')
    x_range = numpy.arange(0., 1.001, .001)
    ax.plot(x_range, merit(x_range, sigma, scale), '--')
    x_range = numpy.arange(-1., 0.001, .001)-1
    ax.plot(x_range, merit(x_range, sigma, scale), '--')
    ax.axvline(x=0, linestyle=':', color='y')
    ax.set_ylim([-0.1, 1.1])
    ax.set_xlim([-2, 1])
    ax.set_title(r'$\tanh\left(c\cdot\frac{t}{\sigma}\right)$')


# test maximum plots
def overMaximum():

    max_merit = lambda values, max_val: Range(values).maximum(max_val=max_val)
    min_merit = lambda values, min_val: Range(values).minimum(min_val=min_val)

    y_vals = np.hstack((np.arange(0, 10, 0.2),
                        np.arange(10, -0.001, -0.2)))
    fig, ax = plt.subplots(figsize=(12, 12),
                           nrows=1, ncols=1,
                           facecolor='white')
    plt.plot(max_merit(y_vals, max_val=None),
             'k--',
             label='$maximum$')
    max_val = 7.
    plt.plot(max_merit(y_vals, max_val=max_val),
             '-.',
             label='$max_{val}=%.1f$' % max_val)
    plt.plot(Increase(y_vals).tomax(max_val=max_val),
             '-.',
             label='$tomax$')
    plt.plot(Astronomy().altitude(y_vals, min_alt=2., max_alt=8.),
             ':',
            label='$altitude$')
    ax.set_ylim([-0.1, 1.1])
    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(0.9, 0.5))

    y_vals = -y_vals
    fig, ax = plt.subplots(figsize=(12, 12),
                           nrows=1, ncols=1,
                           facecolor='white')
    plt.plot(min_merit(y_vals, min_val=None),
             'k--',
             label='$minimum$')
    min_val = -7.
    plt.plot(min_merit(y_vals, min_val=min_val),
             '-.',
             label='$min_{val}=%.1f$' % min_val)
    ax.set_ylim([-0.1, 1.1])
    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(0.9, 0.5))


# test script approaching a limit
def limitApproach():
    merit = lambda x_range, start, limit, alpha: Decrease(x_range).approach(start=start, limit=limit, alpha=alpha)

    def set_axis(ax):
        ax.set_ylim([-0.1, 1.1])
        ax.invert_xaxis()
        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(0.9, 0.5))

    fig, ax = plt.subplots(figsize=(12, 12),
                           nrows=2, ncols=1,
                           facecolor='white')
    x_range = np.arange(10., 0., -0.1)  # deg
    alpha = 1.
    ax[0].plot(x_range, merit(x_range, 7., 3., alpha),
               linestyle='--',
               label=r'$\alpha=%.1f$' % alpha)
    alpha = 2.
    ax[0].plot(x_range, merit(x_range, 7., 3., alpha),
               linestyle='--',
               label=r'$\alpha=%.1f$' % alpha)
    ax[0].plot(x_range, merit(x_range, None, 3., alpha),
               linestyle='--',
               label=r'$\alpha=%.1f$' % alpha)
    set_axis(ax[0])

    x_range = np.arange(15., 5., -0.1)  # deg
    alpha = 3.
    ax[1].plot(x_range, merit(x_range, 13., 3., alpha),
               linestyle='--',
               label=r'$\alpha=%.1f$' % alpha)
    ax[1].plot(x_range, merit(x_range, None, 3., alpha),
               linestyle='--',
               label=r'$\alpha=%.1f$' % alpha)
    ax[1].set_xlabel(r'$\theta$')
    set_axis(ax[1])


# -fin-
