# flake8: noqa
import numpy as np


## Library functions
# set all values=1 after some number
def extend(x_range, merit, value=1., max_value=None, min_value=None):
    x_range = np.asarray(x_range)
    merit = np.asarray(merit)
    if min_value is not None:
        merit[x_range < min_value] = value
    if max_value is not None:
        merit[x_range > max_value] = value
    return merit


# set all values=0 after some number
def trim(x_range, merit, min_value=None, max_value=None):
    x_range = np.asarray(x_range)
    merit = np.asarray(merit)
    if min_value is not None:
        merit[x_range < min_value] = 0
    if max_value is not None:
        merit[x_range > max_value] = 0
    return merit


# sets outlier to 0 and 1 boundary of merit
def normalise(merit):
    if merit.size > 1:
        merit[merit < 0] = 0
        merit[merit > 1] = 1
    else:
        merit = np.min((1., np.max((0., merit))))
    return merit


# scales input values to all be -1 to 1
def scale(values, scale_val=None):
    if scale_val is None:
        return values/np.abs(values).max()
    else:
        return values/np.abs(scale_val)


# scales input values positive
def shift(x_range):
    return x_range - x_range.min()


# scales input values around the mean
def center(x_range):
    return x_range - x_range.mean()


# scale and center input values around 0
def scalecenter(x_range):
    x_range = np.array(x_range)
    x_range = (x_range - x_range.mean())
    return scale(x_range)


# find nearest item in numpy array
def find_nearest(array, value):
    idx = (np.abs(array-value)).argmin()
    return array[idx]


# make sure the input is an array
def make_array(eval_in):
    eval_in = np.asarray(eval_in)
    if eval_in.size < 2:
        eval_in = np.asarray([eval_in])
    return eval_in


# Class for special merits associated with astronomy calculations
class Astronomy:
    def __init__(self):
        pass

    # Merit function
    def airmass(self, zenith_angle, model='secz', steepness=1.):
        zenith_angle = np.asarray(zenith_angle)
        if model == 'pickering':
            # Pickering (2002)
            # airmass=1./np.sin(np.deg2rad(h+244/(165+47*h**1.1)))
            h = 90. - zenith_angle  # deg
            z = np.sin(np.deg2rad(h+244/(165+47*h**1.1)))
        else:  # default is sec z approximation
            # X = sec z (inverse easy to calculate)
            # airmass=1./np.cos(np.deg2rad(zenith_angle))
            z = np.cos(np.deg2rad(zenith_angle))
        return (z**steepness)

    def altitude(self, altitude, min_alt, max_alt):
        merit = ((np.asarray(altitude) - min_alt) / (max_alt - min_alt))
        return normalise(merit)

class Instrument:
    def __init__(self, eval_in=None):
        self.eval_in = eval_in
        # handle both values and arrays
        if eval_in is not None:
            self.eval_in = make_array(eval_in)

    def slew(self, obstime):
        return obstime / (self.eval_in + obstime)



# Class maxima and minima for input values (independent of range)
class Range:
    def __init__(self, eval_in=None):
        self.eval_in = eval_in
        # handle both values and arrays
        if eval_in is not None:
            self.eval_in = make_array(eval_in)

    # find maximum of a curve
    def maximum(self, max_val=None, alpha=1.):
        merit = np.abs(scale(self.eval_in, scale_val=max_val)**alpha)
        minlim = np.argmin(merit[:merit.size/2])
        merit = trim(self.eval_in,
                     merit,
                     min_value=self.eval_in[minlim])
        return normalise(merit)

    # find minimum of a curve
    def minimum(self, min_val=None, alpha=1.):
        merit = scale(self.eval_in, scale_val=min_val)**alpha
        maxlim = np.argmax(merit[:merit.size/2])
        merit = extend(self.eval_in,
                       merit,
                       max_value=self.eval_in[maxlim])
        return normalise(merit + 1.)

    # periodic with sharp spike around desired time
    def spike(self, delta, factor=3.):
        a = 1.+10.**-float(factor)
        x_range = ((self.eval_in - delta/2.)%delta - delta/2.)
        return ((a**(-1.*np.sign(x_range)))**x_range)

    # phase coherence with periodic function
    def coherence(self, ninterval=None, sigma=1.):
        if ninterval is not None:
            eval_out = 2.*np.pi*((self.eval_in-ninterval/2.)%ninterval)/ninterval
        else:
            eval_out = 2.*np.pi*self.eval_in
        return np.exp(-(center(eval_out)/sigma)**2)

    # simple periodic box function
    def box(self, ninterval, fwhm=0.3):
        eval_out = ((self.eval_in % ninterval) / ninterval)
        mask = (eval_out < fwhm)
        shift_width = int(np.abs(np.nonzero(mask)[0][0] - np.nonzero(~mask)[0][0])/2.) + 1
        mask = np.roll(mask, -shift_width)
        eval_out[mask] = 1.
        eval_out[~mask] = 0.
        return eval_out

    # function bends around some center/boundary
    def inflection(self, alpha=1., center=True):
        if center:
            self.eval_in = scalecenter(self.eval_in)
        if (alpha < 1):
            eval_out = np.zeros(np.shape(self.eval_in))
            eval_out[self.eval_in >= 0] = self.eval_in[self.eval_in >= 0]**alpha
            eval_out[self.eval_in < 0] = -abs(self.eval_in[self.eval_in < 0])**alpha
            return eval_out
        return (self.eval_in ** alpha)


# Class for all decreasing functions
class Decrease:
    def __init__(self, eval_in=None):
        self.eval_in = eval_in
        # handle both values and arrays
        if eval_in is not None:
            self.eval_in = make_array(eval_in)

    # smooth decrease over range
    def inflection(self, alpha=1.):
        if self.eval_in.min() < 0:  # needs to be positive
            self.eval_in = shift(self.eval_in)
        self.eval_in = scale(self.eval_in)
        return normalise(Range(1.-self.eval_in).inflection(alpha=alpha, center=False))

    # piece wise linear decrease
    def genfall(self, delay=0., norm_val=1.):
        merit = (norm_val + delay - self.eval_in)/norm_val
        return normalise(merit)

    # piece wise linear decrease between two values
    def pwlinear(self, min_lim=None, max_lim=None):
        if max_lim is None:
            max_lim = np.max(self.eval_in)
        if min_lim is None:
            min_lim = np.min(self.eval_in)
        merit = (max_lim - self.eval_in)/(max_lim - min_lim)
        return normalise(merit)

    # decrease to zero at some value
    def tomin(self, lim_val=None, alpha=1.):
        return 1.-Increase(self.eval_in).tomax(lim_val=lim_val, alpha=alpha)

    # decrease to a limit from a value
    def approach(self, limit, alpha=1.):
        merit = ((limit - self.eval_in)/limit)**alpha
        return trim(self.eval_in, merit, max_value=limit)

    # decrease to the left start from a value with zero at the limit
    def approachleft(self, limit, start=0., alpha=1.):
        merit = ((self.eval_in - limit)/(start - limit))**alpha
        return normalise(trim(self.eval_in, merit, min_value=limit))

    # smooth downward crossing over a boundary value
    def xboundary(self, alpha=1., deltat=1.):
        return Increase(self.eval_in).xboundary(alpha=-alpha,
                                                deltat=deltat)


# Class for all increasing functions
class Increase:
    def __init__(self, eval_in=None):
        self.eval_in = eval_in
        # handle both values and arrays
        if eval_in is not None:
            self.eval_in = make_array(eval_in)

    # smooth increase over range
    def inflection(self, alpha=1.):
        self.eval_in = shift(scale(self.eval_in))
        return normalise(Range(self.eval_in).inflection(alpha=alpha, center=False))

    # piece wise linear increase between two values
    def pwlinear(self, min_lim=None, max_lim=None):
        merit = Decrease(self.eval_in).pwlinear(min_lim, max_lim)
        # return normalise(merit[::-1])
        return normalise(1. - merit)

    # increase to 1 at max value
    def tomax(self, lim_val=None, alpha=1.):
        merit = (scale(self.eval_in, scale_val=lim_val))**alpha
        return normalise(merit)

    # increase to 1 as limit is approached
    def approach(self, limit, deltat=0., alpha=1.):
        if self.eval_in is None:
            self.eval_in = deltat
        merit = (1.-scale((limit - self.eval_in), scale_val=limit))**alpha
        if merit.size > 1:
            merit = extend(self.eval_in, merit, max_value=limit)
        return merit

    # increase over window toward the end
    def window(self, a, b, c):
        merit = (-a*self.eval_in + b/(1+c*self.eval_in))
        return normalise(merit)

    # smooth upward crossing over a boundary value
    def xboundary(self, alpha=1., deltat=1.):
        if self.eval_in is None:
            self.eval_in = deltat
        merit = np.tanh(self.eval_in/alpha)
        return 0.5*merit+0.5


# Class simple polynomial outputs
class sPoly:
    def __init__(self):
        self.rank_range = np.array([1., 2., 3., 4.], dtype=float)

    # standard inversion
    def invert(self, alpha=1.):
        return (1./self.eval_in)**alpha

    def flatrank(self, rank, weight=1., mean=False):
        weight = np.asarray(weight, dtype=float)
        if np.any(weight > 1):
            raise RuntimeError('All weights must be < 1')
        rank = np.asarray(rank, dtype=float)/max(self.rank_range)
        if weight.size > 1:
            if weight.size != rank.size:
                raise RuntimeError('Number weights not equal to number ranks')
        if mean:
            return np.mean(weight*rank)
        return weight*rank


# Generate a table showing all the types of merit function options
if __name__ == '__main__':
    import sys
    import matplotlib.pylab as plt

## piecewise linear decreasing boundary crossing
    x_range = np.arange(0., 1., 0.001)
    fig, ax = plt.subplots(nrows=1, ncols=1, facecolor='white')
    wait = 0.1
    max_time = 0.7
    merit = Decrease(x_range).genfall(delay=wait, norm_val=max_time)
    ax.plot(x_range, merit,
            'b--',
            label=r'$(1 + \frac{t_{wait}-t}{t_{max}})$')
    merit = Decrease(x_range).pwlinear(min_lim=wait, max_lim=max_time)
    ax.plot(x_range, merit,
            'b:',
            label=r'$(\frac{t_{max}-t}{t_{max}-t_{wait}})$')
    ax.set_ylim([-0.01, 1.01])
    ax.set_xlabel('Time [sec]')
    ax.set_ylabel('Piecewise linear decreasing')
    ax.set_title(r'$t_{wait}=%.1f\,[sec]\ t_{max}=%.1f\,[sec]$'
                 % (wait, max_time))

    if len(sys.argv) > 1:
        import libtest
## Boundary crossing
        # verify inflection crossing implementation
        libtest.boundaryInflection()
        # verify smooth boundary crossing implementation
        libtest.boundaryCross(down=True)
        libtest.boundaryCross(rise=True)
        # verify piecewise linear implementation
        libtest.piecewiseLinear(rise=True)
        libtest.piecewiseLinear(fall=True)
        # verify maximum crossing
        libtest.overMaximum()
## Boundary approaching
        # verify decreasing merit
        libtest.boundaryApproach(decrease=True)
        # verify increasing merit
        libtest.boundaryApproach(increase=True)
        # verify decreasing to a set value
        libtest.limitApproach()
## Coherence
        # verify periodic merit
        libtest.phaseCoherence()

## Merit matrix with defaults
    import matplotlib.gridspec as gridspec
    fig = plt.figure(figsize=(12, 12), facecolor='white')
    G = gridspec.GridSpec(7, 3)
    pos_plot = lambda x, y, label: plt.plot(x, y, 'k--', label=r'%s' % label)
    lin_plot = lambda x, y, label: plt.plot(x, y, 'k:', label=r'%s' % label)
    neg_plot = lambda x, y, label: plt.plot(x, y, 'k-.', label=r'%s' % label)

    # max value
    ax = plt.subplot(G[0, 0])
    plt.xticks(())
    plt.yticks(())
    plt.text(0.5, 0.5, 'Maxima',
             ha='center', va='center',
             size=24, alpha=.5)
    ax.axis('off')
    ax = plt.subplot(G[0, 1], frameon=False)
    x_range = np.arange(-10, 10.002, 0.2)
    y_vals = np.hstack((np.arange(0, 10, 0.2),
                        np.arange(10, -0.002, -0.2)))
    plt.plot(x_range, Range(y_vals).maximum(),
             'k--',
             label='$max$')
    max_val = 7.
    plt.plot(x_range, Range(y_vals).maximum(max_val=max_val),
             'k-.',
             label='$lim_{val}=%.1f$' % max_val)
    plt.plot(x_range, Astronomy().altitude(y_vals, min_alt=2., max_alt=8.),
             'k:',
             label='$alt$')
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    ax = plt.subplot(G[0, 2], frameon=False)
    x_range = np.arange(-10, 10.002, 0.2)
    y_vals = np.hstack((np.arange(0, -10, -0.2),
                        np.arange(-10, 0.001, 0.2)))
    plt.plot(x_range, Range(y_vals).minimum(),
             'k--',
             label='$min$')
    min_val = -7.
    plt.plot(x_range, Range(y_vals).minimum(min_val=min_val),
             'k-.',
             label='$lim_{val}=%.1f$' % min_val)
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')

    # decrease
    ax = plt.subplot(G[1, 0])
    plt.xticks(())
    plt.yticks(())
    plt.text(0.5, 0.5, 'Decreasing',
             ha='center', va='center',
             size=24, alpha=.5)
    ax.axis('off')
    ax = plt.subplot(G[1, 1], frameon=False)
    x_range = np.arange(0., 1.001, .001)
    alpha = 0.5
    pos_plot(x_range,
             Decrease(x_range).inflection(alpha=alpha),
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 1.
    lin_plot(x_range,
             Decrease(x_range).inflection(alpha=alpha),
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 2
    neg_plot(x_range,
             Decrease(x_range).inflection(alpha=alpha),
             label=r'$\alpha=%.1f$' % alpha)
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    ax = plt.subplot(G[1, 2], frameon=False)
    x_range = np.arange(0., 1.001, .001)
    fall_from = 0.1
    fall_to = 0.7
    merit = Decrease(x_range).genfall(delay=fall_from, norm_val=fall_to)
    plt.plot(x_range, merit,
             'k-.',
             label=r'$range$')
    merit = Decrease(x_range).pwlinear(min_lim=fall_from, max_lim=fall_to)
    plt.plot(x_range, merit,
             'k--',
             label=r'$range$')
    plt.plot(x_range, Decrease(x_range).pwlinear(),
             'k:',
             label=r'$linear$')
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')

    # increase
    ax = plt.subplot(G[2, 0])
    plt.xticks(())
    plt.yticks(())
    plt.text(0.5, 0.5, 'Increasing',
             ha='center', va='center',
             size=24, alpha=.5)
    ax.axis('off')
    ax = plt.subplot(G[2, 1], frameon=False)
    x_range = np.arange(0., 1.001, .001)
    alpha = 0.5
    pos_plot(x_range,
             Increase(x_range).inflection(alpha=alpha),
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 1.
    lin_plot(x_range,
             Increase(x_range).inflection(alpha=alpha),
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 2
    neg_plot(x_range,
             Increase(x_range).inflection(alpha=alpha),
             label=r'$\alpha=%.1f$' % alpha)
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    ax = plt.subplot(G[2, 2], frameon=False)
    x_range = np.arange(-0.5, 0.501, .001)
    rise_from = -0.3
    rise_to = 0.3
    merit = Increase(x_range).pwlinear(min_lim=rise_from, max_lim=rise_to)
    plt.plot(x_range, merit,
             'k--',
             label=r'$range$')
    plt.plot(x_range, Increase(x_range).pwlinear(),
             'k:',
             label=r'$linear$')
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')

    # approach
    ax = plt.subplot(G[3, 0])
    plt.xticks(())
    plt.yticks(())
    plt.text(0.5, 0.5, 'Approach',
             ha='center', va='center',
             size=24, alpha=.5)
    ax.axis('off')
    ax = plt.subplot(G[3, 1], frameon=False)
    x_range = np.arange(0., 1.001, 0.001)
    alpha = 0.4
    plt.plot(x_range, Increase(x_range).tomax(lim_val=0.7, alpha=alpha),
             'k--',
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 1
    plt.plot(x_range, Increase(x_range).tomax(alpha=alpha),
             'k--',
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 3
    plt.plot(x_range, Increase(x_range).tomax(alpha=alpha),
             'k-.',
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 10
    plt.plot(x_range, Increase(x_range).tomax(lim_val=np.max(x_range), alpha=alpha),
             'k:',
             label=r'$\alpha=%.1f$' % alpha)
    tratio = np.arange(1, -0.01, -0.01)
    a = 1.; b = 1.; c = 0.
    plt.plot(tratio[::-1], Increase(tratio).window(a, b, c),
             'k--')
    a = 0.25; b = 1.; c = 3.
    plt.plot(tratio[::-1], Increase(tratio).window(a, b, c),
             'k-.')
    a = 0.; b = 1.; c = 50.
    plt.plot(tratio[::-1], Increase(tratio).window(a, b, c),
             'k:')
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    ax = plt.subplot(G[3, 2], frameon=False)
    x_range = np.arange(0., 1.001, 0.001)
    alpha = 1
    plt.plot(x_range, Decrease(x_range).tomin(lim_val=np.max(x_range), alpha=alpha),
             'k--',
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 3
    plt.plot(x_range, Decrease(x_range).tomin(lim_val=np.max(x_range), alpha=alpha),
             'k-.',
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 10
    plt.plot(x_range, Decrease(x_range).tomin(lim_val=np.max(x_range), alpha=alpha),
             'k:',
             label=r'$\alpha=%.1f$' % alpha)
    x_range = np.arange(0., 1.001, 0.001)
    alpha = 1
    plt.plot(x_range, Decrease(x_range).approach(1., alpha=alpha),
             'k--',
             label=r'$approach\ 1,\ \alpha=%.1f$' % alpha)
    plt.plot(0.5, Decrease(0.5).approach(1., alpha=alpha),
             'k*')
    alpha = 3
    plt.plot(x_range, Decrease(x_range).approach(0.7, alpha=alpha),
             'k-.',
             label=r'$approach\ 0.7,\ \alpha=%.1f$' % alpha)
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')

    # boundary
    ax = plt.subplot(G[4:6, 0])
    plt.xticks(())
    plt.yticks(())
    plt.text(0.5, 0.5, 'Boundary Crossing',
             ha='center', va='center',
             size=24, alpha=.5)
    ax.axis('off')

    ## gaussian
    ax = plt.subplot(G[4, 1], frameon=False)
    x_range = np.arange(0., 72, 0.1)
    sigma = 1.
    merit = Range(x_range).coherence(12, sigma=sigma)
    ax.plot(x_range, merit,
            'k--',
            label=r'$\sigma=%.2f$' % sigma)
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    ax = plt.subplot(G[4, 2], frameon=False)
    x_range = np.arange(0., 72, 0.1)
    fwhm = 0.3
    merit = Range(x_range).box(12, fwhm=fwhm)
    ax.plot(x_range, merit,
            'k--',
            label=r'$\fwhm=%.2f$' % fwhm)
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    # # sharp
    # x_range = np.arange(-2., 2.001, .001)
    # a = 3.
    # plt.plot(x_range, ((a**(-1.*np.sign(x_range)))**x_range))

    ## smooth
    ax = plt.subplot(G[5, 1], frameon=False)
    x_range = np.arange(-1., 1.001, .001)
    alpha = 0.1
    merit = Increase(x_range).xboundary(alpha=alpha)
    ax.plot(x_range, merit,
            'k--',
            label=r'$\alpha=%.2f$' % alpha)
    alpha = 0.2
    merit = Increase(x_range).xboundary(alpha=alpha)
    ax.plot(x_range, merit,
            'k-.',
            label=r'$\alpha=%.2f$' % alpha)
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    ax = plt.subplot(G[5, 2], frameon=False)
    x_range = np.arange(-1., 1.001, .001)
    alpha = 0.1
    merit = Decrease(x_range).xboundary(alpha=alpha)
    ax.plot(x_range, merit,
            'k--',
            label=r'$\alpha=%.2f$' % alpha)
    alpha = 0.4
    merit = Decrease(x_range).xboundary(alpha=alpha)
    ax.plot(x_range, merit,
            'k-.',
            label=r'$\alpha=%.2f$' % alpha)
    ax.axis('off')
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')

    # specials
    ax = plt.subplot(G[6, 0])
    plt.xticks(())
    plt.yticks(())
    plt.text(0.5, 0.5, 'Specials',
             ha='center', va='center',
             size=24, alpha=.5)
    ax.axis('off')
    plt.tight_layout()
    ## airmass
    ax = plt.subplot(G[6, 1], frameon=False)
    elevation = np.arange(1., 90.)  # deg
    zenith_angle = 90. - elevation
    steepness = 1.
    merit = Astronomy().airmass(zenith_angle, model='secz', steepness=steepness)
    plt.plot(elevation, merit,
             'k--',
             label=r'$\sec(z_h)$')
    steepness = 4.
    merit = Astronomy().airmass(zenith_angle, model='secz', steepness=steepness)
    plt.plot(elevation, merit,
            'k:',
             label=r'$\sec(z_h)$')
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    ## fairness (inflection -- rising only)
    ax = plt.subplot(G[6, 2], frameon=False)
    x_range = np.arange(-1, 0.001, 0.001)
    alpha = 0.5
    pos_plot(x_range,
             Range(x_range).inflection(alpha=alpha),
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 1.
    lin_plot(x_range,
             Range(x_range).inflection(alpha=alpha),
             label=r'$\alpha=%.1f$' % alpha)
    alpha = 3
    neg_plot(x_range,
             Range(x_range).inflection(alpha=alpha),
             label=r'$\alpha=%.1f$' % alpha)
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('meritmatrix.png', facecolor=fig.get_facecolor(), transparent=True)

    plt.show()
# -fin-
