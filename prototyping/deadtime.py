# flake8: noqa

from scipy import special
import numpy
import pylab

def slew_time(
              distance, # angular distance deg
              rate,  # slew rate deg/sec
              ramptime = -1,  # time to achieve slew rate sec
             ):

    if ramptime < 0:
        # default assume constant speed
        time = distance/rate
    else:
        # distance to full rate
        dist = rate * ramptime
        accel = rate/ramptime
        if distance < dist:
            time = 2.*numpy.square(0.5*distance/accel)
        else:
            time = 2.*numpy.square(dist/accel) + (distance-2.*dist)/rate

    return time


def deadtimepenalty(Tobs,  # Total observation time [seconds]
                    Tdead, # Total time not capturing data [seconds]
                    b=1,
                   ):
    Tobs = float(Tobs)
    Tdead = numpy.array(Tdead, dtype=float)
    # return 1.-(Tdead/Tobs)**b
    # if b < 0:
    #     Tperc = (Tobs-Tdead)/Tobs
    Tperc=1.-Tdead/Tobs
    Tperc[Tperc<=0]=0
    return Tperc**b

def rate_function(
                  angular_distance, # degree
                  r = 1.,           # full rate deg/sec
                  a = 1.,           # ramp-up slope parameter
                  b = None,         # ramp_down slope parameter
                  kreep = 0.1,      # deg/sec
                  kreep_angle = 1., # degree
                  ):
    if b is None: b = a # symmetry in accel/decel slopes
    if angular_distance < kreep_angle: # degree
        return [angular_distance, kreep]
    angle = numpy.arange(0, angular_distance, angular_distance/1000.)[:1000] # degrees
    fup = special.erf(angle[:int(len(angle)/2)]/(numpy.sqrt(2.)*a)) - numpy.sqrt(2./numpy.pi)*(angle[:int(len(angle)/2)]*numpy.exp(-angle[:int(len(angle)/2)]**2/(2.*a**2)))/a
    fdown = special.erf(angle[:int(len(angle)/2)]/(numpy.sqrt(2.)*b)) - numpy.sqrt(2./numpy.pi)*(angle[:int(len(angle)/2)]*numpy.exp(-angle[:int(len(angle)/2)]**2/(2.*b**2)))/b
    f = numpy.hstack([fup, fdown[::-1]])
    rate = r*f
    rate[numpy.where(angle < kreep_angle)] = kreep
    return [angle, rate]

# def slew_merit(slew_time, c=1.):
#     if not (0 < c <=1):
#         raise RuntimeError('Incorrect range: 0 < c <=1')
#     slew_time = numpy.array(slew_time)+1
#     return c*(1./slew_time)
def slew_merit(slew_time, creep_time, max_slew_time=120):
    slew_time = numpy.array(slew_time)
    merit = numpy.min((1, numpy.max((0, 1+(creep_time-slew_time)/max_slew_time))))
    return merit

from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


if __name__ == '__main__':

    import sys

    # time allocation
    if sys.argv[1] == 'slewmerit':
        creep_rate = 0.1 # deg/sec
        creep_angle = 1 # deg
        creep_time = creep_angle/creep_rate
        angle = numpy.arange(0, 10., 0.1) # degrees
        slew_time_merit_60 = numpy.zeros(angle.shape)
        slew_time_merit_120 = numpy.zeros(angle.shape)
        time = []
        for idx, dist in enumerate(angle):
            # assume motor reaches full rate of 5 deg/s after 10 sec
            time.append(slew_time(dist, 5., 10.))
            slew_time_merit_60[idx] = slew_merit(time[-1],
                                                 creep_time=creep_time,
                                                 max_slew_time = 60)
            slew_time_merit_120[idx] = slew_merit(time[-1], creep_time=creep_time)
        time=numpy.asarray(time)
        pylab.figure(figsize=(8,8))
        pylab.plot(time, slew_time_merit_60, 'r-.', label='Max slew time 60 seconds')
        pylab.plot(time, slew_time_merit_120, 'b--', label='Max slew time 120 seconds')
        pylab.title('Slew merit')
        pylab.xlabel('Slew time [sec]')
        pylab.ylabel(r'$(1 + \frac{t_{creep}-t_{slew}}{t_{max}})$')
        pylab.legend(loc=0)
        # ##Show impact of kreep angle selection
        # pylab.figure(figsize=(8,12))
        # slew_distance = 15.
        # distance = numpy.arange(0, slew_distance, 0.01)
        # clrs = ['b', 'r', 'g', 'k']
        # kreeps = [2., 1., 0.5, 0.1]
        # for idx in range(len(kreeps)):
        #     kreep_angle = kreeps[idx]
        #     ra_rate=[]
        #     dec_rate=[]
        #     for slew_distance in distance:
        #         a=2.3
        #         b=2.
        #         [x, y] = rate_function(slew_distance, r=5., a=a, b=b, kreep=0.1, kreep_angle=kreep_angle) # RA axis
        #         ra_rate.append(numpy.mean(y))
        #         a=2.3
        #         [x, y] = rate_function(slew_distance, r=10., a=a, kreep=0.2, kreep_angle=kreep_angle) # DEC axis
        #         dec_rate.append(numpy.mean(y))
        #     pylab.subplot(211)
        #     pylab.plot(distance, numpy.array(distance)/numpy.array(ra_rate), clrs[idx], label=r'Kreep %.1f deg'%kreep_angle)
        #     pylab.title('RA axis with kreep rate of 0.1 deg/sec')
        #     pylab.xlabel('Slew distance [deg]')
        #     pylab.ylabel('Slew time [sec]')
        #     pylab.legend(loc=0)
        #     pylab.subplot(212)
        #     pylab.plot(distance, numpy.array(distance)/numpy.array(dec_rate), clrs[idx], label=r'Kreep %.1f deg'%kreep_angle)
        #     pylab.title('DEC axis with kreep rate of 0.2 deg/sec')
        #     pylab.xlabel('Slew distance [deg]')
        #     pylab.ylabel('Slew time [sec]')
        #     pylab.legend(loc=0)
        # ##Show per axis slew time
        # ra_rate=[]
        # dec_rate=[]
        # distance = numpy.arange(0,5,0.1)
        # for slew_distance in distance:
        #     a=2.3
        #     b=2.
        #     [x, y] = rate_function(slew_distance, r=5., a=a, b=b, kreep=0.1) # RA axis
        #     ra_rate.append(numpy.mean(y))
        #     a=2.3
        #     [x, y] = rate_function(slew_distance, r=10., a=a, kreep=0.2) # DEC axis
        #     dec_rate.append(numpy.mean(y))
        # slew_time = numpy.sqrt((distance/numpy.array(ra_rate)**2)+(distance/numpy.array(dec_rate)**2))
        # slew_time = numpy.max([distance/numpy.array(ra_rate), distance/numpy.array(dec_rate), slew_time], axis=0)
        # pylab.figure(figsize=(8,12))
        # pylab.subplot(211)
        # pylab.plot(distance, numpy.array(distance)/numpy.array(ra_rate), 'b',
        #            label=r'$T_{ra} = \frac{\Delta RA [deg]}{<r_{RA}(\theta)>[deg/s]}$')
        # pylab.plot(distance, numpy.array(distance)/numpy.array(dec_rate), 'r',
        #            label=r'$T_{dec} = \frac{\Delta DEC [deg]}{<r_{DEC}(\theta)>[deg/s]}$')
        # pylab.plot(distance, slew_time, 'k--',
        #            label=r'$T_s = |T_{ra,dec}|$')
        # pylab.xlabel('Slew distance [deg]')
        # pylab.ylabel('Slew time [s]')
        # pylab.legend(loc=0)
        # pylab.subplot(212)
        # for c in [1, 0.7, 0.5]:
        #     pylab.plot(distance, slew_merit(slew_time,c=c), '.-', label='c=%.1f'%c)
        # pylab.xlabel('Slew distance [deg]')
        # pylab.ylabel('Slew merit')
        # pylab.legend(loc=0)

        # ##Show slew time in 2D
        # ra = numpy.linspace(0,360,1000)
        # dec = numpy.linspace(0,90,1000)
        # X, Y = numpy.meshgrid(dec, ra)
        # ts=numpy.zeros(X.shape)
        # for ra_slew_idx in range(len(ra)):
        #     ra_slew_distance = ra[ra_slew_idx]
        #     a=2.3
        #     b=2.
        #     [x, y] = rate_function(ra_slew_distance, r=5., a=a, b=b, kreep=0.1) # RA axis
        #     ra_slew_rate = numpy.mean(y)
        #     ra_slew_time = float(ra_slew_distance)/ra_slew_rate
        #     for dec_slew_idx in range(len(dec)):
        #         dec_slew_distance = dec[dec_slew_idx]
        #         a=2.3
        #         [x, y] = rate_function(dec_slew_distance, r=10., a=a, kreep=0.2) # DEC axis
        #         dec_slew_rate = numpy.mean(y)
        #         dec_slew_time = float(dec_slew_distance)/dec_slew_rate
        #         ts[dec_slew_idx,ra_slew_idx]=numpy.sqrt(ra_slew_time**2+dec_slew_time**2)
        # #Show slew merit
        # fig=plt.figure(figsize=(9,9))
        # ax = fig.add_subplot(111,projection='3d')
        # ax.plot_surface(X, Y, slew_merit(ts), alpha=0.4, cmap=cm.coolwarm, linewidth=0, antialiased=False)
        # ax.set_zlabel('Slew merit')
        # ax.set_xlabel('DEC [deg]')
        # ax.set_ylabel('RA [deg]')
        # plt.axis('tight')

    if sys.argv[1] == 'penalty':
        ##Show various options for penalising dark time
        Tobs = 60.
        # Tdead = numpy.linspace(0,Tobs)
        Tdead = numpy.arange(0,Tobs+1, Tobs/1000.)
        pylab.figure(facecolor='white')
        # for b in [0.5, 1, 5]:
        for b in [0.5, 1, 2]:
            print Tdead/Tobs*100
            pylab.plot(Tdead/Tobs*100, deadtimepenalty(Tobs, Tdead, b), label=r'b=%.1f'%b)
        pylab.xlabel('Dark Time [%]')
        pylab.ylabel('Dead Time Penalty')
        pylab.title('Percentage dead time for an observation')
        pylab.legend(loc=0)
        pylab.axis('tight')


    pylab.show()



# -fin-
