from scipy import special
import numpy
import pylab


def rate_function(
                  angular_distance,  # degree
                  r=1.,              # full rate deg/sec
                  a=1.,              # ramp-up slope parameter
                  b=None,            # ramp_down slope parameter
                  kreep=0.1,         # deg/sec
                  kreep_angle=1.,    # degree
                  ):
    if b is None:
        b = a  # symmetry in accel/decel slopes
    if angular_distance < kreep_angle:  # degree
        return [angular_distance, kreep]
    angle = numpy.arange(0, angular_distance, angular_distance/1000.)[:1000]  # degrees
    fup = special.erf(angle[:int(len(angle)/2)]/(numpy.sqrt(2.)*a)) - numpy.sqrt(2./numpy.pi)*(angle[:int(len(angle)/2)]*numpy.exp(-angle[:int(len(angle)/2)]**2/(2.*a**2)))/a
    fdown = special.erf(angle[:int(len(angle)/2)]/(numpy.sqrt(2.)*b)) - numpy.sqrt(2./numpy.pi)*(angle[:int(len(angle)/2)]*numpy.exp(-angle[:int(len(angle)/2)]**2/(2.*b**2)))/b
    f = numpy.hstack([fup, fdown[::-1]])
    rate = r*f
    rate[numpy.where(angle < kreep_angle)] = kreep
    return [angle, rate]


def slew_merit(slew_time, c=1.):
    if not (0 < c <= 1):
        raise RuntimeError('Incorrect range: 0 < c <=1')
    slew_time = numpy.array(slew_time)+1
    return c*(1./slew_time)

# ## -- MAIN -- ###
# #Show per axis rate
pylab.figure(figsize=(8, 8))
for slope in [1., 2., 5., 10.]:
    [x, y] = rate_function(15, a=slope, kreep=0., kreep_angle=0.)
    pylab.plot(x[:len(y)/2], y[:len(y)/2], label='a=%d' % slope)
pylab.axis('tight')
pylab.legend(loc=0)
pylab.title('Accel/Decel rates')
pylab.xlabel('Slew distance [deg]')
pylab.ylabel('Rate [deg/sec]')

# #Show rate vs distance using rate function
pylab.figure(figsize=(8, 12))
for slew_distance in [50., 20., 5., 2.]:
    pylab.subplot(211)
    a = 2.3
    b = 2.
    [x, y] = rate_function(slew_distance, r=5., a=a, b=b, kreep_angle=0, kreep=0)  # RA axis
    pylab.plot(x, y, '-', label=r'$\Delta\theta=%d$ degrees' % slew_distance)
    pylab.title('RA axis motion')
    pylab.xlabel('Slew distance [deg]')
    pylab.ylabel('Rate [deg/sec]')
    pylab.legend(loc=0)
    pylab.subplot(212)
    a = 2.3
    [x, y] = rate_function(slew_distance, r=10., a=a, kreep=0, kreep_angle=0.)  # DEC axis
    pylab.plot(x, y, label=r'$\Delta\theta=%d$ degrees' % slew_distance)
    pylab.title('DEC axis motion')
    pylab.xlabel('Slew distance [deg]')
    pylab.ylabel('Rate [deg/sec]')
    pylab.legend(loc=0)

# #Show axis motor rate graph
pylab.figure(figsize=(8, 12))
slew_distance = 20.
pylab.subplot(211)
a = 2.3
b = 2.
[x, y] = rate_function(slew_distance, r=5., a=a, b=b, kreep=0.1)  # RA axis
pylab.plot(x, y, '-', label=r'a =%.1f, b =%.1f' % (a, b))
pylab.title('RA axis with kreep rate of 0.1 deg/sec for < 1 degree')
pylab.xlabel('Slew distance [deg]')
pylab.ylabel('Rate [deg/sec]')
pylab.legend(loc=0)
pylab.subplot(212)
a = 2.3
[x, y] = rate_function(slew_distance, r=10., a=a, kreep=0.2)  # DEC axis
pylab.plot(x, y, label=r'a = b = %.1f' % a)
pylab.title('DEC axis with kreep rate of 0.2 deg/sec for < 1 degree')
pylab.xlabel('Slew distance [deg]')
pylab.ylabel('Rate [deg/sec]')
pylab.legend(loc=0)

# #Show impact of kreep angle selection
pylab.figure(figsize=(8, 12))
slew_distance = 15.
distance = numpy.arange(0, slew_distance, 0.01)
clrs = ['b', 'r', 'g', 'k']
kreeps = [2., 1., 0.5, 0.1]
for idx in range(len(kreeps)):
    kreep_angle = kreeps[idx]
    ra_rate = []
    dec_rate = []
    for slew_distance in distance:
        a = 2.3
        b = 2.
        [x, y] = rate_function(slew_distance, r=5., a=a, b=b, kreep=0.1, kreep_angle=kreep_angle)  # RA axis
        ra_rate.append(numpy.mean(y))
        a = 2.3
        [x, y] = rate_function(slew_distance, r=10., a=a, kreep=0.2, kreep_angle=kreep_angle)  # DEC axis
        dec_rate.append(numpy.mean(y))
    pylab.subplot(211)
    pylab.plot(distance, numpy.array(distance)/numpy.array(ra_rate), clrs[idx], label=r'Kreep %.1f deg' % kreep_angle)
    pylab.title('RA axis with kreep rate of 0.1 deg/sec')
    pylab.xlabel('Slew distance [deg]')
    pylab.ylabel('Slew time [sec]')
    pylab.legend(loc=0)
    pylab.subplot(212)
    pylab.plot(distance, numpy.array(distance)/numpy.array(dec_rate), clrs[idx], label=r'Kreep %.1f deg' % kreep_angle)
    pylab.title('DEC axis with kreep rate of 0.2 deg/sec')
    pylab.xlabel('Slew distance [deg]')
    pylab.ylabel('Slew time [sec]')
    pylab.legend(loc=0)

# #Show per axis slew time
ra_rate = []
dec_rate = []
# distance = numpy.arange(0, 15, 0.1)
distance = numpy.arange(0, 5, 0.1)
for slew_distance in distance:
    a = 2.3
    b = 2.
    [x, y] = rate_function(slew_distance, r=5., a=a, b=b, kreep=0.1)  # RA axis
    ra_rate.append(numpy.mean(y))
    a = 2.3
    [x, y] = rate_function(slew_distance, r=10., a=a, kreep=0.2)  # DEC axis
    dec_rate.append(numpy.mean(y))
slew_time = distance/numpy.array(ra_rate)+distance/numpy.array(dec_rate)
# slew_time = numpy.max([distance/numpy.array(ra_rate),distance/numpy.array(dec_rate)], axis=0)
pylab.figure(figsize=(8, 12))
pylab.subplot(211)
pylab.plot(distance, numpy.array(distance)/numpy.array(ra_rate), 'b',
           label=r'$T_{ra} = \frac{\Delta RA [deg]}{<r_{RA}(\theta)>[deg/s]}$')
pylab.plot(distance, numpy.array(distance)/numpy.array(dec_rate), 'r',
           label=r'$T_{dec} = \frac{\Delta DEC [deg]}{<r_{DEC}(\theta)>[deg/s]}$')
pylab.plot(distance, slew_time, 'k--',
           label=r'$T_s = T_{ra} + T_{dec}$')
pylab.xlabel('Slew distance [deg]')
pylab.ylabel('Slew time [s]')
pylab.legend(loc=0)
pylab.subplot(212)
pylab.plot(distance, slew_merit(slew_time))
pylab.xlabel('Slew distance [deg]')
pylab.ylabel('Slew merit')

pylab.figure(figsize=(8, 8))
for c in [1, 0.7, 0.5]:
    pylab.plot(distance, slew_merit(slew_time, c=c), '.-', label='c=%.1f' % c)
pylab.xlabel('Slew distance [deg]')
pylab.ylabel('Slew merit')
pylab.legend(loc=0)

pylab.show()
import sys
sys.exit(0)

# #Show slew time in 2D
ra = numpy.linspace(0, 360, 1000)
dec = numpy.linspace(0, 90, 1000)
X, Y = numpy.meshgrid(dec, ra)
ts = numpy.zeros(X.shape)
for ra_slew_idx in range(len(ra)):
    ra_slew_distance = ra[ra_slew_idx]
    a = 2.3
    b = 2.
    [x, y] = rate_function(ra_slew_distance, r=5., a=a, b=b, kreep=0.1)  # RA axis
    ra_slew_rate = numpy.mean(y)
    ra_slew_time = float(ra_slew_distance)/ra_slew_rate
    for dec_slew_idx in range(len(dec)):
        dec_slew_distance = dec[dec_slew_idx]
        a = 2.3
        [x, y] = rate_function(dec_slew_distance, r=10., a=a, kreep=0.2)  # DEC axis
        dec_slew_rate = numpy.mean(y)
        dec_slew_time = float(dec_slew_distance)/dec_slew_rate
        ts[dec_slew_idx, ra_slew_idx] = ra_slew_time + dec_slew_time
from matplotlib import cm
# from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(9, 9))
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(X, Y, ts, alpha=0.4, cmap=cm.coolwarm, linewidth=0, antialiased=False)
ax.set_zlabel('Slew time')
ax.set_xlabel('DEC [deg]')
ax.set_ylabel('RA [deg]')
plt.axis('tight')

# #Show slew merit
fig = plt.figure(figsize=(9, 9))
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(X, Y, slew_merit(ts), alpha=0.4, cmap=cm.coolwarm, linewidth=0, antialiased=False)
ax.set_zlabel('Slew merit')
ax.set_xlabel('DEC [deg]')
ax.set_ylabel('RA [deg]')
plt.axis('tight')

pylab.show()

# -fin-
