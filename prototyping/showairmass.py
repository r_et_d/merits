import ephem
import numpy
import matplotlib.pylab as plt

##Zenith angle, the angle between a direction of interest and the local zenith
##Local zenith refers to an imaginary point directly "above" a particular location
step = 0.1
elevation = numpy.arange(90.-step, 0.-step, -step)  #deg
zenith_angle = numpy.arange(step, 90.+step, step) #deg

##Zenith limit
# Highest elevation limit for telescope operations
zenith_elev_max = 85 #deg

##Airmass
# Pickering (2002)
fig = plt.figure()
ax = plt.subplot(111)
# X = sec z (inverse easy to calculate)
airmass = 1./numpy.cos(numpy.deg2rad(elevation))
plt.plot(elevation, airmass, 'b', label='sec z')
# Pickering (inversion estimated from basic fit)
h = 90. - zenith_angle #deg
airmass = 1./numpy.sin(numpy.deg2rad(h+244/(165+47*h**1.1)))
plt.plot(zenith_angle, airmass, 'r', label='Pickering')

airmass_limit = 10.
plt.axhline(y=airmass_limit, color='y', linestyle='--')
X = numpy.rad2deg(numpy.arccos(1./airmass_limit))
text = ' X = sec z = 10 @ zenith angle = %.2f deg' % numpy.rad2deg(numpy.arccos(1./airmass_limit))
plt.axvline(x=X, color='m', linestyle='--')
ax.annotate(text,
            xy=(0.45, 0.31),
            xytext=(0.2, 0.5),    # fraction, fraction
            xycoords='figure fraction',
            arrowprops=dict(arrowstyle="->"),
            horizontalalignment='middel',
            verticalalignment='bottom',)

X = zenith_angle[numpy.argmin(numpy.abs(airmass-airmass_limit))]
text = ' X = Pickering = 10 @ zenith angle = %.2f deg' % zenith_angle[numpy.argmin(numpy.abs(airmass-airmass_limit))]
plt.axvline(x=X, color='m', linestyle='--')
ax.annotate(text,
            xy=(0.5, 0.31),
            xytext=(0.2, 0.15),    # fraction, fraction
            xycoords='figure fraction',
            arrowprops=dict(arrowstyle="->"),
            horizontalalignment='middel',
            verticalalignment='bottom',)

plt.axis([80, 90, 0, max(airmass)])
plt.xlabel('Apparent Zenith Angle [Deg]')
plt.ylabel('Airmass')
plt.legend(loc=0)

airmass_limit = 5.
print ' X = sec z = %.2f @ zenith = %.2f deg' % (airmass_limit, numpy.rad2deg(numpy.arccos(1./airmass_limit)))
print ' X = Pickering = %.2f @ zenith = %.2f deg' % (airmass_limit, zenith_angle[numpy.argmin(numpy.abs(airmass-airmass_limit))])
airmass_limit = 1.3
print ' X = sec z = %.2f @ zenith = %.2f deg' % (airmass_limit, numpy.rad2deg(numpy.arccos(1./airmass_limit)))
print ' X = Pickering = %.2f @ zenith = %.2f deg' % (airmass_limit, zenith_angle[numpy.argmin(numpy.abs(airmass-airmass_limit))])


plt.show()
# -fin-
# vim:sw=4:
