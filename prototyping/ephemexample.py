# http://rhodesmill.org/pyephem/quick.html
# http://rhodesmill.org/pyephem/tutorial.html

import ephem
from pprint import pprint
import datetime
import numpy

# List all known planets and moons
pprint(ephem._libastro.builtin_planets())
pprint([name for _0, _1, name in ephem._libastro.builtin_planets()])

## When using PyEphem, you will usually create instances of the bodies that
## interest you, compute their position for various dates and perhaps geographic
## locations

# dates are expressed as year, month, and day, delimited by slashes
today = datetime.date.today()
today_str = today.strftime('%Y/%m/%d')
print 'Date string format for pyephem', today_str

# getting to pyephem objects
moon_obj = (getattr(ephem, 'Moon'))
moon = moon_obj()
print moon
moon = ephem.Moon()
print moon
moon.compute(today_str)

# building you local position
observer = ephem.Observer()
observer.lon = '21:24:38.5'
observer.lat = '-30:43:17.3'
observer.elevation = 1038.0
observer.date = ephem.now()
print 'longitude', str(observer.lon), float(observer.lon), numpy.rad2deg(float(observer.lon))
print 'latitude', str(observer.lat), float(observer.lat), numpy.rad2deg(float(observer.lat))

moon = ephem.Moon(observer)
print 'Percent of surface illuminated', moon.moon_phase
print 'Percent of surface illuminated', moon.phase
print 'Moon rise time', moon.rise_time
print 'Apparent position relative to horizon: alt, az', numpy.rad2deg(float(moon.alt)), moon.az
print 'Apparent topocentric position: ra,dec', moon.ra, moon.dec

# phases and illumination factors
pnm = ephem.previous_new_moon(ephem.now())
nnm = ephem.next_new_moon(ephem.now())
print 'New moon illumination', ephem.Moon(pnm).moon_phase, ephem.Moon(nnm).moon_phase
pfm = ephem.previous_full_moon(ephem.now())
nfm = ephem.next_full_moon(ephem.now())
print 'Full moon illumination', ephem.Moon(pfm).moon_phase, ephem.Moon(nfm).moon_phase


# find rising, setting and transit time
print 'Next rise', observer.next_rising(ephem.Moon(ephem.now()))
print 'Next set', observer.next_setting(ephem.Moon())
print 'Next transit', observer.next_transit(ephem.Moon())

# define your own celestial targer as a fixedbody
hyda = ephem.FixedBody()
hyda.name = 'Hydra A'
hyda._ra = '9:18:05.28'
hyda._dec = '12:05:48.9'
hyda.compute(observer)
print 'Target', hyda, hyda.name, hyda.ra, hyda.dec

# measure the angle between any pair of spherical coordinates
print 'Separation angle between moon and Hydra A', ephem.separation(moon, hyda)
sep_angle = numpy.rad2deg(ephem.separation(moon, hyda))
print 'Separation angle between moon and Hydra A', sep_angle

# rise and set times of target
print 'Target Hydra A will rise', hyda.rise_time
print 'Target Hydra A will set', hyda.set_time

# lst rise and set times
observer.date = hyda.rise_time
print observer.sidereal_time()
hyda.compute(observer)
print numpy.rad2deg(hyda.alt)
observer.date = hyda.set_time
print observer.sidereal_time()
