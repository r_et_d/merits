# flake8: noqa

import ephem
import datetime
import numpy
from matplotlib import rc_file
rc_file('pgf.rc')
import matplotlib.dates as mdates
import matplotlib.pylab as plt

from figsize import figsize


instrument_min_elevation_angle = 15.  # deg
instrument_zenith_pointing_limit = 88.  # deg

def horizon_mask(azimuth):
    return False  # no horizon mask

#1) get the minimum elevation angle in the azimuth direction defined by
#   the horizon mask using the minimum elevation over the visibility period
def lowest_elevation(observer, target, Tstart, Tend, limit=False):
    observer.date = Tstart
    target.compute(observer)
    rise_horizon = numpy.max([horizon_mask(numpy.rad2deg(float(target.az))), numpy.rad2deg(float(target.alt))])
    observer.date = Tend
    target.compute(observer)
    set_horizon = numpy.max([horizon_mask(numpy.rad2deg(float(target.az))), numpy.rad2deg(float(target.alt))])
    minelevation = [numpy.rad2deg(observer.horizon),
                    instrument_min_elevation_angle,
                    rise_horizon, set_horizon]

    if limit:
        return numpy.max(minelevation)
    return minelevation

#2) get the maximum elevation angle limits
def highest_elevation(observer, target, Tstart, limit=False):
    maxelevation = [instrument_zenith_pointing_limit]
    Ttrans = observer.next_transit(target, start=Tstart)
    observer.date = Ttrans
    target.compute(observer)
    maxelevation.append(numpy.rad2deg(float(target.alt)))
    if limit:
        return numpy.min(maxelevation)
    return maxelevation


if __name__ == '__main__':

    # building you local position
    observer = ephem.Observer()
    observer.lon = '21:24:38.5'
    observer.lat = '-30:43:17.3'
    observer.elevation = 1038.0
    # set horizon as the minimum elevation angle defined by the instrument
    observer.horizon = ephem.degrees(str(numpy.max([0., instrument_min_elevation_angle])))
    observer.date = '2016/11/22 12:00:00'

    # define observation period
    # Night start time
    Nstart = observer.next_setting(ephem.Sun())
    # Night end time
    Nend = observer.next_rising(ephem.Sun())
    print Nstart, Nend

    # define an example celestial target as a fixedbody
    target = ephem.FixedBody()
    target.name = 'Dummy'
    target._ra = '3:48:05.28'
    target._dec = '13:00:00.0'

    rise_time = observer.next_rising(target)
    set_time = observer.next_setting(target, start=rise_time)
    Tstart = max([Nstart, rise_time])
    Tend = min([Nend, set_time])

    minelevation = lowest_elevation(observer, target, Tstart, Tend, limit=True)
    maxelevation = highest_elevation(observer, target, Tstart, limit=True)

#Soft limit
    # position Evaluation
    position_merit = []
    dtime = []
    # All evaluations related to elevation angle (relative to local observer)
    mins = datetime.timedelta(minutes = 30)
    Tnow = Tstart.datetime()
    while Tnow <= Tend.datetime()+mins:
        observer.date = Tnow
        target.compute(observer)
        h_eval = (numpy.rad2deg(float(target.alt)) - minelevation)/ (maxelevation - minelevation)
        position_merit.append(max([0., h_eval]))
        dtime.append(Tnow)
        Tnow = Tnow + mins

    fig, ax = plt.subplots(facecolor='white', figsize=figsize(0.95),
                           constrained_layout=True)
    # fig.autofmt_xdate()
    plt.plot(dtime, position_merit)
    # xfmt = mdates.DateFormatter('%d-%m-%y %H:%M')
    xfmt = mdates.DateFormatter('%H:%M')
    ax.xaxis.set_major_formatter(xfmt)
    # plt.gcf().autofmt_xdate()  # reduces size (!)
    plt.setp(plt.xticks()[1], rotation=30, ha='right')
    # plt.xlabel('Observation time [datetime]')
    plt.xlabel('Observation time [night]')
    # plt.ylabel(r'$\frac{h - \min(\Delta h)}{\max(\Delta h)}$')
    # plt.title('Position merit')
    plt.ylabel('Position merit')
    plt.savefig('position-merit.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('position-merit.pgf')
    plt.show()

# -fin-
