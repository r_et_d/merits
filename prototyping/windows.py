# flake8: noqa

import ephem
import numpy
import matplotlib.pylab as plt


##0<=TANH<=1
# edge evaluation merit base function
def edge_mrt_base(sigma=1., scale=10.):
    dt_br = 5
    step = 0.01
    rt_br = numpy.arange(-dt_br/2, dt_br/2+step, step)
    # time_interval = numpy.array(time_interval, dtype=float)
    return numpy.tanh(float(scale)*rt_br/float(sigma))


##Startup merit
# Variable that rise from a certain level to one within a specified time
def mrt_rise(ephem_rise_time=None, win_secs=300, ephem_date_time=None, sigma=1., scale=10., test=False):
    merit = 0.5*edge_mrt_base(sigma, scale)+0.5
    if test:
        return merit

    t0 = float(ephem_rise_time)
    start_time = t0-float(win_secs/2.*ephem.second)
    end_time = t0+float(win_secs/2.*ephem.second)
    step = (end_time-start_time)/len(merit)
    win_range = numpy.arange(start_time, end_time, step)

    if ephem_date_time is None:
        ephem_date_time = ephem_rise_time
    ephem_date_time = float(ephem_date_time)

    date_idx = numpy.argmin(numpy.abs(float(ephem_date_time)-win_range))
    return merit[date_idx]


##Terminate merit
# Variable that drops from a certain level to zero within a specified time
def mrt_set(ephem_set_time=None, win_secs=300, ephem_date_time=None, sigma=1., scale=10., test=False):
    merit = 0.5*edge_mrt_base(-sigma, scale)+0.5
    if test:
        return merit

    t0 = float(ephem_set_time)
    start_time = t0-float(win_secs/2.*ephem.second)
    end_time = t0+float(win_secs/2.*ephem.second)
    step = (end_time-start_time)/len(merit)
    win_range = numpy.arange(start_time, end_time, step)

    if ephem_date_time is None:
        ephem_date_time = ephem_set_time
    ephem_date_time = float(ephem_date_time)

    date_idx = numpy.argmin(numpy.abs(float(ephem_date_time)-win_range))
    return merit[date_idx]


## Window merit
# Variable that rises when the time remaining to observe this target in the
# current night drops.
def win_mrt_base(tr, a, b, c):
    return (-a*tr + b/(1+c*tr))
def mrt_win(tr, a, b, c=10.):
    return (numpy.append(0, win_mrt_base(tr, a, b, c))).max()


if __name__ == '__main__':
    import sys

    sigma = 1.
    scale = 10.
    brt = numpy.arange(len(mrt_rise(sigma=sigma, test=True)))
    brt = (brt-len(brt)/2.)/max(brt)

    if sys.argv[1] == 'rise':
        lnstl=['--','-.']
        # start merit -- favours target as it nears rise time
        # plt.figure(figsize=(9, 6))
        fig = plt.figure(facecolor='white')
        fig.autofmt_xdate()
        for idx, sigma in enumerate(range(1, 11, 9)):
            plt.plot(brt, mrt_rise(sigma=sigma, test=True),
                     linestyle=lnstl[idx], linewidth=2,
                     label=r'$\sigma=%d$' % sigma)
            # plt.title(r'Startup merit using $tanh(x)$')
            # plt.ylabel(r'$mrt_{rise}(t)$')
            plt.ylabel('Startup merit')
            # plt.xlabel(r'$t_0-t$')
            plt.xlabel(r'$\Delta t$', fontsize=16)
            plt.ylim([-0.01, 1.01])
            plt.axis('tight')
            plt.legend(loc=0, prop={'size': 16})
        plt.savefig('rise-boundary.png', facecolor=fig.get_facecolor(), transparent=True)

    if sys.argv[1] == 'set':
        # end merit -- drops weight as the target nears set time
        lnstl=['--','-.']
        # plt.figure(figsize=(9, 6))
        fig = plt.figure(facecolor='white')
        fig.autofmt_xdate()
        for idx, sigma in enumerate(range(1, 11, 9)):
            plt.plot(brt, mrt_set(sigma=sigma, test=True),
                     linestyle=lnstl[idx], linewidth=2,
                     label=r'$\sigma=%d$' % sigma)
            # plt.title(r'Terminate merit using $tanh(-x)$')
            plt.ylabel(r'Termination merit')
            # plt.ylabel(r'$mrt_{set}(t)$')
            # plt.xlabel(r'$t_0-t$')
            plt.xlabel(r'$\Delta t$', fontsize=16)
            plt.ylim([-0.01, 1.01])
            plt.axis('tight')
            plt.legend(loc=0, prop={'size': 16})
        plt.savefig('set-boundary.png', facecolor=fig.get_facecolor(), transparent=True)

    if sys.argv[1] == 'window':
        # window merit -- increases selection weight at the target observation window shortens
        step = 0.01
        tratio = numpy.arange(1, -step, -step)
        # plt.figure(figsize=(13, 7))
        fig = plt.figure(facecolor='white')
        fig.autofmt_xdate()
        a = 1.; b = 1.; c = 0.
        # plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio], 'r:', label=r'$a$=%.1f,$b$=%.1f,$c$=%.1f' % (a, b, c))
        plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio],
                 'b--', linewidth=2,
                 label=r'$a=%.1f,\,b=%.1f,\,c=%.1f$' % (a, b, c))

        # a = 0.25; b = 1.; c = 3.
        # plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio], 'g--', label=r'$a$=%.1f,$b$=%.1f,$c$=%.1f' % (a, b, c))

        a = 0.; b = 1.; c = 10.
        # plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio], 'b:', label=r'$a$=%.1f,$b$=%.1f,$c$=%.1f' % (a, b, c))
        plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio],
                 'g-.', linewidth=2,
                 label=r'$a=%.1f,\,b=%.1f,\,c=%.1f$' % (a, b, c))
        # a = 0.; b = 1.; c = 50.
        # plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio], 'b--', label=r'$a$=%.1f,$b$=%.1f,$c$=%.1f' % (a, b, c))

        # a = 1.; b = 0.
        # plt.plot(tratio, [mrt_win(tr, a, b) for tr in tratio], 'k', label=r'$a$=%.1f,$b$=%.1f' % (a, b))

        # plt.ylabel(r'$gain(tr)$')
        plt.ylabel(r'Window merit')
        # plt.xlabel(r'$\frac{t_{target}}{t_{night}}$')
        plt.xlabel(r'Time [fraction]')
        # plt.title(r'Window merit')
        plt.axis([tratio[0], tratio[-1], -0.01, 1.01])
        plt.legend(loc=0, prop={'size': 16})
        plt.savefig('target-window-boundary.png', facecolor=fig.get_facecolor(), transparent=True)

    plt.show()

# -fin-
