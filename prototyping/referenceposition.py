# flake8: noqa
import ephem
import numpy
import time
import matplotlib.pylab as plt
from datetime import datetime

def get_sky_ref(target_list, earliest=False, latest=False):
    risetimes = [float(ephem.Date(target.rise_time)) for target in target_list]

    if earliest:
        return target_list[numpy.argmin(risetimes)]

    if latest:
        return target_list[numpy.argmax(risetimes)]

    targets = numpy.array([[target.ra.real, target.dec.real] for target in target_list])
    target = ephem.FixedBody()
    target._ra = numpy.median(targets[:,0])
    target._dec = numpy.median(targets[:,1])

    return target


if __name__ == '__main__':

    observer = ephem.Observer()
    observer.lon = '21:24:38.5'
    observer.lat = '-30:43:17.3'
    observer.elevation = 1038.0
    observer.date = ephem.Date(datetime.now())

    # target=(ra,dec)
    target_list = [
                    ['12:58:55.21', '27:42:33.2'],
                    ['14:43:57.35', '12:41:21.3'],
                    ['16:35:31.48', '-7:20:54.3'],
                  ]

    targets = []
    for idx, target in enumerate(target_list):
        ra, dec = target
        # define your own celestial target as a fixedbody
        target = ephem.FixedBody()
        target._ra = ra
        target._dec = dec
        target.compute(observer)
        print 'target%d'%(idx+1), target.ra, target.dec
        print '\trise time', target.rise_time
        print '\ttransit time and altitude', target.transit_time, numpy.rad2deg(float(target.transit_alt))
        targets.append(target)
    print

    # earliest rise time
    sky_ref_pos = get_sky_ref(targets, earliest=True)
    print 'earliest', sky_ref_pos.ra, sky_ref_pos.dec
    print '\trise time', sky_ref_pos.rise_time
    print '\ttransit time and altitude', sky_ref_pos.transit_time, numpy.rad2deg(float(sky_ref_pos.transit_alt))

    # latest rise time
    sky_ref_pos = get_sky_ref(targets, latest=True)
    print 'latest  ', sky_ref_pos.ra, sky_ref_pos.dec
    print '\trise time', sky_ref_pos.rise_time
    print '\ttransit time and altitude', sky_ref_pos.transit_time, numpy.rad2deg(float(sky_ref_pos.transit_alt))

    # mean rise time - (default) centroid calculated coordinate
    sky_ref_pos = get_sky_ref(targets)
    sky_ref_pos.compute(observer)
    print 'centroid', sky_ref_pos.ra, sky_ref_pos.dec
    print '\trise time', sky_ref_pos.rise_time
    print '\ttransit time and altitude', sky_ref_pos.transit_time, numpy.rad2deg(float(sky_ref_pos.transit_alt))


    plt.figure(facecolor='white')
    ax1 = plt.axes(frameon=False)
    clrs = ['c', 'b', 'g']
    for idx, target in enumerate(targets):
        plt.scatter(numpy.rad2deg(target.ra.real), numpy.rad2deg(target.dec.real), 50, marker='o', color=clrs[idx], label='target%d (%s,%s)'%(idx, target.ra, target.dec))
    plt.scatter(numpy.rad2deg(sky_ref_pos.ra.real), numpy.rad2deg(sky_ref_pos.dec.real), 150, marker='+', color='r', label='centroid (%s,%s)'%(sky_ref_pos.ra, sky_ref_pos.dec))
    plt.grid('on')
    plt.legend(loc=0)
    plt.ylabel('DEC [deg]')
    plt.xlabel('RA [deg]')
    plt.show()


# -fin-
