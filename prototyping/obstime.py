# flake8: noqa

# merit to weight longer or short observations higher
# this merit is retired because it is not really necessary and observation time
# should determine if an observation is scheduled or not

# this is mainly for radio where longer observations is prefered,
# but that is to get UV coverage, and a merit related to
# UV coverage will be more appropriate

import numpy
import pylab

def deltaobsmerit(Tobs, # Total observation time [seconds]
                  Nd,   # Duration of observation period [seconds]
                  b=1,
                  longobs=False,
                  shortobs=False
                 ):
    Nd = float(Nd)
    Tobs = numpy.array(Tobs, dtype=float)
    if longobs:
        return (Tobs/Nd)**b
    if shortobs:
        # return deadtimepenalty(Nd, Tobs, b)
        return 1.-(Tobs/Nd)**b
    raise RuntimeError('Nothing to do, no reason to call this function')


if __name__ == '__main__':

    ##Show merits to favour longer or shorter observations
    Nd = 8*3600
    Tobs = numpy.linspace(0,Nd)
    pylab.figure(figsize=[12,5])
    for b in [1, 3, 10]:
        pylab.subplot(121)
        pylab.plot(Tobs/3600., deltaobsmerit(Tobs, Nd, b, longobs=True), label=r'b=%.1f'%b)
        pylab.subplot(122)
        pylab.plot(Tobs/3600., deltaobsmerit(Tobs, Nd, b, shortobs=True), label=r'b=%.1f'%b)
    pylab.subplot(121)
    pylab.xlabel('Total observation time [hours]')
    pylab.ylabel('Long observation merit')
    pylab.title('8 hours observation period')
    pylab.legend(loc=0)
    pylab.subplot(122)
    pylab.xlabel('Total observation time [hours]')
    pylab.ylabel('Short observation merit')
    pylab.title('8 hours observation period')
    pylab.legend(loc=0)

    pylab.show()


# -fin-
