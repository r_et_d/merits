# flake8: noqa

import numpy
import matplotlib.pylab as plt


#Veto function
def airmass_limit(airmass_threshold, model='secz'):
    ##Zenith angle, the angle between a direction of interest and the local zenith
    ##Local zenith refers to an imaginary point directly "above" a particular location
    step = 0.1
    zenith_angle = numpy.arange(step, 90.+step, step)  # deg

    if model == 'pickering':
        # Pickering (2002)
        h = 90. - zenith_angle  # deg
        airmass = 1./numpy.sin(numpy.deg2rad(h+244/(165+47*h**1.1)))
        zenith_angle = zenith_angle[numpy.argmin(numpy.abs(airmass-airmass_threshold))]
    else:  # default is sec z approximation
        # X = sec z (inverse easy to calculate)
        zenith_angle = numpy.rad2deg(numpy.arccos(1./airmass_threshold))

    airmass_elev = 90.-zenith_angle
    return airmass_elev  # deg


#Merit function
def airmass(zenith_angle, model='secz', steepness=1):
    if model == 'pickering':
        # Pickering (2002)
        # airmass=1./numpy.sin(numpy.deg2rad(h+244/(165+47*h**1.1)))
        h = 90. - zenith_angle  # deg
        z = numpy.sin(numpy.deg2rad(h+244/(165+47*h**1.1)))
    else:  # default is sec z approximation
        # X = sec z (inverse easy to calculate)
        # airmass=1./numpy.cos(numpy.deg2rad(zenith_angle))
        z = numpy.cos(numpy.deg2rad(zenith_angle))
    airmass = 1./(z**steepness)
    return airmass


if __name__ == '__main__':

    import sys

# Hard limit
    if sys.argv[1] == 'hard':
        airmass_threshold = 10.
        airmass_elev = airmass_limit(airmass_threshold)
        print 'elevation angle at airmass limit', str(airmass_threshold), '=', str(airmass_elev), 'degrees'

        model = 'pickering'
        airmass_value = airmass(90.-airmass_elev, model=model, steepness=1)
        text = ' X = %s = %.1f @ elev angle = %.2f deg' % (model, airmass_value, airmass_elev)
        print text
        model = 'secz'
        airmass_value = airmass(90.-airmass_elev, model=model, steepness=1)
        text = ' X = %s = %.1f @ elev angle = %.2f deg' % (model, airmass_value, airmass_elev)
        print text

    step = 1.
    elevation = numpy.arange(step, 90., step)  # deg

#Airmass curve
    if sys.argv[1] == 'curve':
        fig = plt.figure()
        ax = plt.subplot(111)
        plt.hold(True)
        model = 'pickering'
        for steepness in [1, 2]:
            airmass_curve = [airmass(90.-elev, model=model, steepness=steepness) for elev in elevation]
            plt.plot(elevation, airmass_curve, label=r'%s with $\alpha$ = %d' % (model, steepness))
        model = 'secz'
        for steepness in [1, 4]:
            airmass_curve = [airmass(90.-elev, model=model, steepness=steepness) for elev in elevation]
            plt.plot(elevation, airmass_curve, label=r'%s with $\alpha$ = %d' % (model, steepness))
        plt.hold(False)
        plt.axis([0, 90, 0, 40])
        plt.xlabel('Elevation Angle [Deg]')
        plt.ylabel(r'Airmass=$\frac{1}{z^\alpha}$')
        plt.legend(loc=0)
        # plt.savefig('airmass-curve.png', facecolor=fig.get_facecolor(), transparent=True)

#Airmass merit
    if sys.argv[1] == 'merit':
        lnstl=['--','-.']
        fig = plt.figure(facecolor='white')
        ax = plt.subplot(111)
        plt.hold(True)
        # model = 'pickering'
        # for steepness in [1, 2]:
        #     airmass_merit = [airmass(90.-elev, model=model, steepness=steepness) for elev in elevation]
        #     plt.plot(elevation, 1./numpy.array(airmass_merit), label=r'%s with $\alpha$ = %d' % (model, steepness))
        model = 'secz'
        for idx, steepness in enumerate([1, 4]):
            airmass_merit = [airmass(90.-elev, model=model, steepness=steepness) for elev in elevation]
            plt.plot(elevation, 1./numpy.array(airmass_merit),
                    linestyle=lnstl[idx], linewidth=2,
                    label=r'$z(h)=\sec(z_h),\, \alpha = %d$' % (steepness))
        plt.hold(False)
        plt.ylabel('Airmass merit')
        # plt.title('Airmass merit')
        # plt.ylabel(r'$\frac{1}{z(h)}$')
        plt.xlabel('Altitude [Deg]')
        plt.legend(loc=0, prop={'size': 18})
        plt.savefig('airmass-merit.png', facecolor=fig.get_facecolor(), transparent=True)

    plt.show()


# -fin-
