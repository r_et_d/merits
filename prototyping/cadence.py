# flake8: noqa

from __future__ import print_function
import numpy as np
import matplotlib.pylab as plt


if __name__ == '__main__':

    import sys

    # observation frequency of every 4 hours
    dt = 4. # hours
    dt = 12. # daily
    # dt = 22. # daily
    # dt = 2.*24. # every second day
    # observations over 3 days
    dT = 72.
    t = np.arange(0., dT, 1.)
    mt = np.cos(2.*np.pi*(t%dt)/dt)
    plt.figure()
    plt.plot(t, (1.+mt)/2., '--')

    mt0 = []
    tp = []
    t0 = 3. # now = the date of first observation
    for t in np.arange(t0, dT, 1.):
        tp.append(t)
        ts = (t-t0)%dt
        mt0.append(np.cos(2.*np.pi*ts/dt))
    mt0 = np.asarray(mt0)
    plt.plot(tp, (1.+mt0)/2., ':')

    from datetime import datetime, timedelta
    dt = timedelta(hours=dt).total_seconds()
    now = datetime.now()
    t0 = now + timedelta(hours=-4)
    dp = np.arange(t0, t0+timedelta(hours=dT), timedelta(hours=1)).astype(datetime)
    mt = []
    for t in dp:
        # mt.append(np.cos(2.*np.pi*((t-t0).total_seconds()%dt)/dt))
        theta = 2.*np.pi*((t-t0).total_seconds()%dt)/dt
        mt.append(np.exp(-theta/4.))
    mt = np.asarray(mt)

    plt.figure()
    plt.plot(dp, mt, '--')

    plt.figure()
    t0 = now
    t = np.asarray([ts.total_seconds() for ts in (dp-t0)])
    x = 2.*np.pi*(t%dt)/dt
    x = (x-x.mean())
    mt = np.exp(-x**2)
    plt.plot(dp, mt, '--')

    tv = timedelta(hours=4).total_seconds()/dt
    mt = np.exp(-(x-tv)**2)
    plt.plot(dp, mt, ':')

    mt = np.exp(-((x-tv)/2)**2)
    plt.plot(dp, mt, 'r:')

    plt.show()

# -fin-
