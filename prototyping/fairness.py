import numpy
import matplotlib.pylab as plt


def timefairnessmerit(share, alpha):
    if (alpha < 1):
        timeshare = numpy.zeros(numpy.shape(share))
        timeshare[share >= 0] = share[share >= 0] ** alpha
        timeshare[share < 0] = -abs(share[share < 0]) ** alpha
        return timeshare
    return (share ** alpha)


def timefairness(granted, actual, alpha):
    share = (granted - actual)
    if alpha < 1 and (share < 0):
        return -abs(share)**alpha
    return share**alpha


def timecompletion(Tgranted, alpha):
    Tactual = numpy.arange(0, Tgranted+1)
    dT = Tactual/Tgranted
    # favours completing projects to ensure publications
    return numpy.asarray(dT**alpha)
    # # favours starting projects to ensure wider spread of projects
    # return 1.-numpy.asarray(dT**alpha)


def normaliserank(priority):
    rank = numpy.array([1., 2., 3., 4.], dtype=float)
    return (priority/numpy.max(rank))


def sciencerank(priority, weight=1, mean=False):
    priority = numpy.asarray(weight*priority, dtype=float)
    # mean rank
    if mean:
        return (numpy.mean(normaliserank(priority)))
    # single rank default behaviour
    # all observation have the project priority
    # or all observation individual priorities
    return (normaliserank(priority))


if __name__ == '__main__':

    import sys

    # time allocation
    if sys.argv[1] == 'fairness':
        plt.figure(facecolor='white')
        clrs = ['b', 'g', 'r']
        for idx, alpha in enumerate([0.5, 1, 3]):
            share = numpy.arange(-1, 1, 0.001)
            Tfairness = timefairnessmerit(share, alpha=alpha)
            plt.plot(share, Tfairness, clrs[idx]+':', label=r'$\alpha$=%.1f' % alpha)

            for actual in numpy.arange(0, 1, 0.01):
                # Tactual = actual observation time fraction to date
                # Ttotal = total telescope observation time to date
                # actual = Tactual/Ttotal = total observation time given to date
                # granted = fraction of total telescope time allocated to group
                # evaluate (granted-actual)
                if idx == 0:
                    plt.plot(0.1-actual, timefairness(0.1, actual, alpha), clrs[idx]+'.')
                if idx == 1:
                    plt.plot(0.5-actual, timefairness(0.5, actual, alpha), clrs[idx]+'.')
                if idx == 2:
                    plt.plot(0.9-actual, timefairness(0.9, actual, alpha), clrs[idx]+'.')

        plt.title(r'$m(\%)=(\%_{granted}-\%_{actual})^\alpha$')
        plt.ylabel('Fairness Gain')
        plt.xlabel(r'$\%$')
        plt.legend(loc=0)
        # plt.savefig('timefairness.png', facecolor=fig.get_facecolor(), transparent=True)

    # project completeness
    if sys.argv[1] == 'completion':
        Tgranted = 60.  # seconds
        plt.figure(facecolor='white')
        for alpha in [0.2, 1, 6]:
            Tcompletion = timecompletion(Tgranted, alpha=alpha)
            plt.plot(Tcompletion, label=r'$\alpha$=%.1f' % alpha)
        plt.title(r'$m(\Delta)=(\Delta_{actual}/\Delta_{granted})^\alpha$')
        plt.ylabel('Completion')
        plt.xlabel(r'$\Delta$')
        plt.legend(loc=0)
        # plt.savefig('timecompletion.png', facecolor=fig.get_facecolor(), transparent=True)

    # scientific priority
    if sys.argv[1] == 'priority':
        n_observations = range(5)
        plt.figure(facecolor='white')
        # Single priority for all observations/programs in the project
        plt.subplot(311)
        priority = 2
        normalised_priority = sciencerank(priority)*numpy.ones(numpy.size(n_observations))
        plt.plot(n_observations, normalised_priority, 'b*:', label='Single project priority')
        plt.ylim([0, 1])
        plt.legend(loc=0)
        # Assigned normalised priority per observation/program
        plt.subplot(312)
        priority = [2, 1, 2, 3, 2]
        normalised_priority = sciencerank(priority)
        plt.plot(n_observations, normalised_priority, 'r*:', label='Individual observation priorities')
        plt.ylim([0, 1])
        plt.legend(loc=0)
        plt.ylabel('Scientific Priority')
        # Mean normalised priority per observation/program
        plt.subplot(313)
        plt.plot(n_observations, normaliserank(priority), 'g*')
        normalised_priority = sciencerank(priority, mean=True)*numpy.ones(numpy.size(n_observations))
        plt.plot(n_observations, normalised_priority, 'g.:', label='Mean priority')
        plt.xlabel(r'Observation [#]')
        plt.ylim([0, 1])
        plt.legend(loc=0)
        # plt.savefig('sciencerank.png', facecolor=fig.get_facecolor(), transparent=True)

    plt.show()


# -fin #
