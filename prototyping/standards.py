from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt


# -- mini tools -- #

def ptable(table_array):
    """Fancy print recarray"""
    from prettytable import PrettyTable
    tbl = PrettyTable(table_array.dtype.names)

    table_array.sort(order=table_array.dtype.names)
    for row in table_array:
        tbl.add_row(row)
    print(tbl)


def sptable(table_array):
    """Fancy print sorted recarray."""
    from prettytable import PrettyTable
    tbl = PrettyTable(table_array.dtype.names)

    for row in table_array[table_array['standard'].argsort()]:
        tbl.add_row(row)
    print(tbl)


def sign(number): return (number > 0) - (number < 0)


def HHMMSS2DEG(coord_vec):
    return 15*(float(coord_vec[0]) +
               float(coord_vec[1])/60.0 +
               float(coord_vec[2])/3600.0)


def DDMMSS2DEG(coord_vec):
    return sign(float(coord_vec[0]))*(np.abs(float(coord_vec[0])) +
                                      float(coord_vec[1])/60.0 +
                                      float(coord_vec[2])/3600.0)


def DEG2HHMMSS(ra_deg):
    ra_hrs = float(ra_deg)/15.
    hh = int(ra_hrs)
    mm_rem = (ra_hrs - hh)*60.
    mm = int(mm_rem)
    ss = (mm_rem - mm)*60.
    return [hh, mm, ss]


def DEG2DDMMSS(dec_deg):
    sgn = sign(dec_deg)
    dec_deg = np.abs(float(dec_deg))
    dd = int(dec_deg)
    mm_rem = (dec_deg - dd)*60.
    mm = int(mm_rem)
    ss = (mm_rem - mm)*60.
    return [sgn*dd, mm, ss]

# -- mini tools -- #


# Read catalogue file and build recarray
def read_catalogue(filename):
    import re
    try:
        fin = open(filename, 'r')
        fin.readline()  # drop header
        data = fin.readlines()
        fin.close()
    except IOError:
        raise RuntimeError('Cannot read file %s, '
                           'check location or permissions' % filename)

    table_desc = {
        'names': ('standard', 'ra', 'dec', 'ra_deg', 'dec_deg', 'epoch'),
        'formats': ((str, 10), (str, 10), (str, 10), 'f4', 'f4', 'i4'),
        }
    table = np.recarray(0, dtype=table_desc)

    for line in data:
        values = line.strip().split()
        if not values:  # skip empty lines
            continue
        (name, radec, epoch) = values[:3]
        ra_, sign, dec_ = re.split('([-+])', radec)
        ra = ':'.join(map(''.join, zip(*[iter(ra_)]*2)))
        dec = sign + ':'.join(map(''.join, zip(*[iter(dec_)]*2)))
        table = np.append(
            table,
            np.array([(
                name,
                ra,
                dec,
                HHMMSS2DEG(ra.split(':')),
                DDMMSS2DEG(dec.split(':')),
                float(epoch),
                )],
                dtype=table_desc
                )
            )

    return table


if __name__ == '__main__':
    cat_name = 'Eregionstars_J2000.catalogue'
    catalogue = read_catalogue(cat_name)
    ptable(catalogue)
    # sptable(catalogue)
    fig = plt.figure(figsize=(10, 10))
    axes = fig.add_axes([0.1, 0.1, 0.8, 0.8], polar=True)
    axes.set_ylim(-90, 30)
    axes.set_yticks(np.arange(-90, 30, 15))
    axes.scatter(catalogue['ra_deg'], catalogue['dec_deg'], s=8, marker='.')
    plt.title('E-region stars')
    plt.show()

# -fin-
