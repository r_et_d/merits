# import ephem
import numpy
# import datetime, time
import matplotlib.pylab as plt

##Fuzzy borders
# Define a relaxation buffer (time buffer) to start/end observations
dt_br = 5
t0 = 0.
step = 0.01
rt_br = numpy.arange(t0-dt_br/2, t0+dt_br/2+step, step)
sigma = 1.
scale = 10.


##0<=TANH<=1
# edge evaluation merit base function
def edge_mrt_base(scale, sigma):
    return numpy.tanh(scale*(rt_br-t0)/sigma)


##Startup merit
# Variable that rise from a certain level to one within a specified time
def mrt_rise(sigma, scale=10.):
    return 0.5*edge_mrt_base(scale, sigma)+0.5


##Terminate merit
# Variable that drops from a certain level to zero within a specified time
def mrt_set(sigma, scale=10.):
    return 0.5*edge_mrt_base(scale, -sigma)+0.5


plt.figure(figsize=(13, 10))
for sigma in range(1, 11, 9):
    plt.subplot(211)
    plt.plot(rt_br, mrt_rise(sigma), label=r'$\sigma$=%d' % sigma)
    plt.ylabel(r'$mrt_{rise}(t)$')
    plt.xlabel(r'$t_0-t$')
    plt.title(r'Startup merit using $tanh(x)$')
    plt.axis([rt_br[0], rt_br[-1], -0.01, 1.01])
    plt.legend(loc=0)
    plt.subplot(212)
    plt.plot(rt_br, mrt_set(sigma), label=r'$\sigma$=%d' % sigma)
    plt.title(r'Terminate merit using $tanh(-x)$')
    plt.ylabel(r'$mrt_{set}(t)$')
    plt.xlabel(r'$t_0-t$')
    plt.axis([rt_br[0], rt_br[-1], -0.01, 1.01])
    plt.legend(loc=0)

#GET START AND END TIME USING RISE AND SET ANGLES


##Window merit
# Variable that rises when the time remaining to observe this target in the
# current night drops.
##GET PROPER CALCULATION FOR TR
step = 0.01
tratio = numpy.arange(1, -step, -step)


def win_mrt_base(tr, a, b, c):
    return (-a*tr + b/(1+c*tr))


def mrt_win(tr, a, b, c=10.):
    return (numpy.append(0, win_mrt_base(tr, a, b, c))).max()


plt.figure(figsize=(13, 7))
a = 1.; b = 1.; c = 0.
plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio], 'r:', label=r'$a$=%.1f,$b$=%.1f,$c$=%.1f' % (a, b, c))

a = 0.25; b = 1.; c = 3.
plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio], 'g--', label=r'$a$=%.1f,$b$=%.1f,$c$=%.1f' % (a, b, c))

a = 0.; b = 1.; c = 10.
plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio], 'b:', label=r'$a$=%.1f,$b$=%.1f,$c$=%.1f' % (a, b, c))

a = 0.; b = 1.; c = 50.
plt.plot(tratio, [mrt_win(tr, a, b, c) for tr in tratio], 'b--', label=r'$a$=%.1f,$b$=%.1f,$c$=%.1f' % (a, b, c))

a = 1.; b = 0.
plt.plot(tratio, [mrt_win(tr, a, b) for tr in tratio], 'k', label=r'$a$=%.1f,$b$=%.1f' % (a, b))

plt.ylabel(r'$gain(tr)$')
plt.xlabel(r'$\frac{t_{target}}{t_{night}}$')
plt.title(r'Window merit')
plt.axis([tratio[0], tratio[-1], -0.01, 1.01])
plt.legend(loc=0)

plt.show()
# -fin-
