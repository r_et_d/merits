import ephem

import sys

try:
    coords = sys.argv[1]
except:
    raise

[tag, ra, dec] = coords.split(',')
ra = ra.strip()
dec = dec.strip()

# building you local position
observer = ephem.Observer()
observer.lon = '21:24:38.5'
observer.lat = '-30:43:17.3'
observer.elevation = 1038.0
observer.date = ephem.now()

# define an example celestial target as a fixedbody
target = ephem.FixedBody()
target.name = tag
target._ra = ra
target._dec = dec

target.compute(observer)
print target.rise_time
print target.set_time
# # define your own celestial targer as a fixedbody
# mytarget = ephem.FixedBody()
# mytarget.name = 'target'
# mytarget._ra = '19:39:25.03'
# mytarget._dec = '-63:42:45.7'
# mytarget.compute(observer)
# print mytarget
# print mytarget.rise_time
# print 'Target', mytarget.name, mytarget.ra, mytarget.dec
# print float(mytarget.ra)
# # # print 'Next rise', observer.next_rising(source)
# # # print 'Next set', observer.next_setting(ephem.Moon())
# # # print 'Next transit', observer.next_transit(ephem.Moon())

# # # rise and set times of target
# print 'Target Hydra A will rise', source.rise_time
# print 'Target Hydra A will set', source.set_time

# # # lst rise and set times
# # observer.date=hyda.rise_time
# # print observer.sidereal_time()
# # hyda.compute(observer)
# # print numpy.rad2deg(hyda.alt)
# # observer.date=hyda.set_time
# # print observer.sidereal_time()
