# flake8: noqa

import ephem
import datetime
import numpy
import matplotlib.pylab as plt


#Merit function
def separation(sep_limit=0., gradient=2., sep_range=50.):
    angle_range= numpy.arange(sep_range, 0., -0.1)  # deg
    merit = ((angle_range-sep_limit)/(numpy.max(angle_range)-sep_limit))**gradient
    merit[angle_range < sep_limit] = 0
    return [angle_range, merit]

# Find highest elevation during observation time
def highest_elev(start_time, end_time, transit_time):
    if end_time < transit_time:
        # 'target rising'
        high_elev_time = end_time
    elif start_time > transit_time:
        # 'target setting'
        high_elev_time = start_time
    else:
        # 'target transit'
        high_elev_time = transit_time
    return high_elev_time

# Zenith region zone of avoidance
def avoidance_zone(target, avoider):
    sep_angle_limit = 15.  # deg
    sep_angle = numpy.rad2deg(ephem.separation(target, avoider))
    print 'Separation angle between target and avoider = %.2f (limit %.2f)' % \
          (sep_angle, sep_angle_limit)
    if sep_angle < sep_angle_limit:
        # assuming 15 angular deg per hour: 1 deg = 3600/15 sec
        # *2, in and out
        dt_in_zone = (2.*numpy.abs(sep_angle_limit-sep_angle)/15.)*3600.
        # print 'Spending %.2f sec time in zone' % dt_in_zone
        return dt_in_zone
    return 0.

# Zenith region zone of avoidance
def avoid_zenith(highest_alt, zenith_elev_limit=88.):
    if highest_alt >= zenith_elev_limit:
        return True
    return False


if __name__ == '__main__':

    import sys

    # merit relating separation angle to some set angular limit
    if sys.argv[1] == 'merit':
        lnstl=['--','-.']
        # a = [20., 20., 5.]  # deg separation limit
        # b = [50, 90, 50]
        a = [20., 5.]  # deg separation limit
        b = [50, 50]
        fig = plt.figure(facecolor='white')
        ax = plt.subplot(111)
        plt.hold(True)
        # for i, c in enumerate([2, 2, 3]):
        for i, c in enumerate([2, 3]):
            [sep_angle, sep_merit] = separation(sep_limit=a[i], gradient=c, sep_range=b[i])
            plt.plot(sep_angle, sep_merit,
                     linestyle=lnstl[i], linewidth=2,
                     label=r'$\left(\frac{\theta-%d}{b}\right)^{%d}$' % (a[i], c))
        plt.xlabel('Separation Angle [Deg]')
        # plt.ylabel(r'$\left(\frac{\theta-a}{b}\right)^{c}$')
        # plt.title('Separation Merit')
        plt.ylabel('Separation Merit')
        plt.legend(loc=0, prop={'size': 20})
        plt.savefig('separation-merit.png', facecolor=fig.get_facecolor(), transparent=True)

        plt.show()

    # stats related to a separation or avoidance angle for an observation
    if sys.argv[1] == 'veto':
        observer = ephem.Observer()
        observer.lon = '21:24:38.5'
        observer.lat = '-30:43:17.3'
        observer.elevation = 1038.0

        target = ephem.FixedBody()
        target._ra = '9:18:05.28'
        target._dec = '-32:05:48.9'

        observer.date = ephem.Date(datetime.datetime.now())
        target.compute(observer)
        transit_time = target.transit_time
        start_time = ephem.Date(transit_time - ephem.hour)
        end_time = ephem.Date(transit_time + ephem.hour)
        max_elev_time = highest_elev(start_time, end_time, transit_time)


        # Zenith angle hard limit
        print "\nEvaluate zenith angle hard limit"
        observer.date = ephem.Date(max_elev_time)
        target.compute(observer)
        zenith_elev_limit=85.  # deg
        transit_altitude = numpy.rad2deg(target.alt.real)
        if avoid_zenith(transit_altitude):
            print 'Zenith limit at %.2f [deg] with target highest alt %.2f [deg]' % \
                  (zenith_elev_limit, transit_altitude)
            print 'Cannot observe target'

        # Allow veto violation
        print "\nViolation of separation angle limit"
        observer.date = ephem.Date(datetime.datetime.now())
        target.compute(observer)
        avoider = ephem.FixedBody()
        avoider._ra = '10:18:05.28'
        avoider._dec = '-32:05:48.9'
        avoider.compute(observer)
        time_loss_sec = avoidance_zone(target, avoider)
        if time_loss_sec > 0.:
            dt_obs = (end_time.datetime()-start_time.datetime()).total_seconds()
            print 'Will enter zone of avoidance for %d [sec] of a %d [sec] observation' % \
                  (time_loss_sec, dt_obs)
            perc_time_loss = time_loss_sec/dt_obs*100.
            print 'Spending %.2f %% time in zone' % perc_time_loss
        if perc_time_loss < 20.: print 'Degraded but observing'
        else: print 'Not observing'



# -fin-
