import ephem
import numpy
import numpy as np
import matplotlib.pyplot as plt
from scipy import spatial


def ptable(table_array):
    from prettytable import PrettyTable
    x = PrettyTable(table_array.dtype.names)

    table_array.sort(order=table_array.dtype.names)
    for row in table_array:
        x.add_row(row)
    print x


def DEG2HHMMSS(ra_deg):
    ra_hrs = float(ra_deg)/15.
    hh = int(ra_hrs)
    mm_rem = (ra_hrs - hh)*60.
    mm = int(mm_rem)
    ss = (mm_rem - mm)*60.
    return [hh, mm, ss]


def DEG2DDMMSS(dec_deg):
    sgn = sign(dec_deg)
    dec_deg = numpy.abs(float(dec_deg))
    dd = int(dec_deg)
    mm_rem = (dec_deg - dd)*60.
    mm = int(mm_rem)
    ss = (mm_rem - mm)*60.
    return [sgn*dd, mm, ss]


def sign(number): return cmp(number, 0)


def HHMMSS2DEG(coord_vec):
    return 15*(float(coord_vec[0]) + float(coord_vec[1])/60.0 + float(coord_vec[2])/3600.0)


def DDMMSS2DEG(coord_vec):
    return sign(float(coord_vec[0]))*(numpy.abs(float(coord_vec[0])) + float(coord_vec[1])/60.0 + float(coord_vec[2])/3600.0)


def read_catalogue(filename):
    from numpy import recarray
    import re
    try:
        fin = open(filename, 'r')
        fin.readline()  # drop header
        data = fin.readlines()
        fin.close()
    except IOError:
        raise RuntimeError('Cannot read file %s, check location or permissions' % filename)

    table_desc = {'names': ('standard', 'ra', 'dec', 'ra_deg', 'dec_deg', 'epoch'), 'formats': ('S10', 'S10', 'S10', 'f4', 'f4', 'd4')}
    table = recarray(0, dtype=table_desc)

    for line in data:
        values = line.strip().split()
        # skip empty lines
        if len(values) < 1:
            continue
        [name, radec, epoch], dummy = values[:3], values[3:]
        ra, sign, dec = re.split('([-+])', radec)
        ra = ':'.join(a+b for a, b in zip(ra[::2], ra[1::2]))
        dec = ':'.join(a+b for a, b in zip(dec[::2], dec[1::2]))
        table = numpy.append(table,
                             numpy.array([(
                                 name,
                                 ra,
                                 sign+dec,
                                 HHMMSS2DEG(ra.split(':')),
                                 DDMMSS2DEG((sign+dec).split(':')),
                                 epoch,
                                 )], dtype=table_desc))

    return table


if __name__ == '__main__':
    cat_name = 'Eregionstars_J2000.catalogue'
    catalogue = read_catalogue(cat_name)
    # ptable(catalogue)

##Building you local position
    observer = ephem.Observer()
    observer.lon = '21:24:38.5'
    observer.lat = '-30:43:17.3'
    observer.elevation = 1038.0

##Selection of standards in a radial distance from sky location
    r = 15.  # radial distance [deg]
    random_targets = []
    closest = []
    highest = []
    random = []
    for i in range(7):
        # randomly generate a sky location
        ra_deg = int(numpy.random.uniform(low=1, high=360))
        dec_deg = int(numpy.random.uniform(low=-80, high=20))

        # find all targets within radial distance
        tree = spatial.cKDTree(zip(numpy.array(catalogue['ra_deg']).ravel(), numpy.array(catalogue['dec_deg']).ravel()))
        coordinate = numpy.array([ra_deg, dec_deg], dtype=float).T
        print 'Sky coordinate %s' % str(coordinate)

        [nearest_dist, nearest_idx] = tree.query(coordinate, k=5, eps=0, p=2, distance_upper_bound=r)
        nearest_dist = nearest_dist[~numpy.isinf(nearest_dist)]
        nearest_idx = nearest_idx[~numpy.isinf(nearest_dist)]
        if len(nearest_dist) < 1:
            print 'No standards found within %d [deg] of (%d, %d)' % (r, ra_deg, dec_deg)
            continue
        random_targets.append(coordinate)
        neighbours = numpy.array(tree.data[nearest_idx], dtype=float)

        # select closest standard
        closest_neighbour = neighbours[0, :]
        print 'Closest standard %s' % str(closest_neighbour)
        closest.append(closest_neighbour)

        # select highest standard
        observer.date = ephem.now()
        highest_neighbour = None
        max_alt = 0
        for neighbour in neighbours:
            E_std = ephem.FixedBody()
            [hh, mm, ss] = DEG2HHMMSS(neighbour[0])
            E_std._ra = '%s:%s:%s' % (str(hh).zfill(2), str(mm).zfill(2), str(ss).zfill(2))
            [dd, mm, ss] = DEG2DDMMSS(neighbour[1])
            E_std._dec = '%s:%s:%s' % (str(dd).zfill(2), str(mm).zfill(2), str(ss).zfill(2))
            E_std.compute(observer)
            neighbour_alt = DDMMSS2DEG(numpy.array(str(E_std.alt).split(':'), dtype=float))
            if highest_neighbour is None or max_alt < neighbour_alt:
                highest_neighbour = neighbour
                max_alt = neighbour_alt
        print 'Highest standard %s' % str(highest_neighbour)
        highest.append(highest_neighbour)

        # select random standard to fill in airmass region
        idx = int(numpy.random.uniform(low=0, high=len(neighbours[0, :])))
        random_neighbour = neighbours[idx, :]
        print 'Random standard %s' % str(random_neighbour)
        random.append(random_neighbour)

        print

    random_targets = numpy.array(random_targets)
    closest = numpy.array(closest)
    highest = numpy.array(highest)
    random = numpy.array(random)

    fig = plt.figure(figsize=(10, 10))
    axes = fig.add_axes([0.1, 0.1, 0.8, 0.8], polar=True)
    axes.set_ylim(-90, 30)
    axes.set_yticks(numpy.arange(-90, 30, 15))
    clrs = ['y', 'r', 'b', 'g', 'm', 'c', 'k']
    # scatter(random_targets[:,1], random_targets[:,0], s=8, marker='*')
    for idx in range(len(random_targets[:, 0])):
        axes.scatter(random_targets[idx, 0], random_targets[idx, 1], s=20, marker='*', color=clrs[idx])
        axes.scatter(closest[idx, 0], closest[idx, 1], s=20, marker='+', color=clrs[idx])
        axes.scatter(highest[idx, 0], highest[idx, 1], s=20, marker='.', color=clrs[idx])
        axes.scatter(random[idx, 0], random[idx, 1], s=20, marker='x', color=clrs[idx])
    plt.title('E-region stars')
    plt.legend(['Coordinate', 'Closest', 'Highest', 'Random'], loc=0)
    plt.show()

# -fin-
