# flake8: noqa
import numpy as np

# Generate a table showing all the types of merit function options
if __name__ == '__main__':
    import sys
    from matplotlib import rc_file
    rc_file('pgf.rc')
    import matplotlib.pylab as plt

    from figsize import figsize
    import libmerits


## Merit matrix with defaults
    pos_plot = lambda x, y: plt.plot(x, y, 'k--')
    lin_plot = lambda x, y: plt.plot(x, y, 'k:')
    neg_plot = lambda x, y: plt.plot(x, y, 'k-.')


## FALLING
    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(0., 1.001, .001)
    alpha = 0.5
    pos_plot(x_range,
             libmerits.Decrease(x_range).inflection(alpha=alpha))
    alpha = 1.
    lin_plot(x_range,
             libmerits.Decrease(x_range).inflection(alpha=alpha))
    alpha = 2
    neg_plot(x_range,
             libmerits.Decrease(x_range).inflection(alpha=alpha))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('smoothfallingmatrix.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('smoothfallingmatrix.pgf')

    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(0., 1.001, .001)
    fall_from = 0.1
    fall_to = 0.7
    lin_plot(x_range,
             libmerits.Decrease(x_range).genfall(delay=fall_from, norm_val=fall_to))
    pos_plot(x_range,
             libmerits.Decrease(x_range).pwlinear(min_lim=fall_from, max_lim=fall_to))
    neg_plot(x_range, libmerits.Decrease(x_range).pwlinear())
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('pwfallingmatrix.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('pwfallingmatrix.pgf')


## RISING
    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(0., 1.001, .001)
    alpha = 0.5
    pos_plot(x_range,
             libmerits.Increase(x_range).inflection(alpha=alpha))
    alpha = 1.
    lin_plot(x_range,
             libmerits.Increase(x_range).inflection(alpha=alpha))
    alpha = 2
    neg_plot(x_range,
             libmerits.Increase(x_range).inflection(alpha=alpha))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('smoothrisingmatrix.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('smoothrisingmatrix.pgf')

    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(-0.5, 0.501, .001)
    rise_from = -0.3
    rise_to = 0.3
    pos_plot(x_range,
             libmerits.Increase(x_range).pwlinear(min_lim=rise_from, max_lim=rise_to))
    lin_plot(x_range,
             libmerits.Increase(x_range).pwlinear())
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('pwrisingmatrix.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('pwrisingmatrix.pgf')


## APPROACH
    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(0., 1.001, 0.001)
    alpha = 3
    plt.plot(x_range, libmerits.Increase(x_range).tomax(alpha=alpha),
             'k-.',
             label=r'$\alpha=%.1f$' % alpha)
    x_range = np.arange(0., 1.001, 0.001)
    alpha = 8.
    pos_plot(x_range,
             libmerits.Increase(x_range).approach(1., alpha=alpha))
    alpha = 1.
    lin_plot(x_range,
             libmerits.Increase(x_range).approach(1., alpha=alpha))
    alpha = 0.4
    neg_plot(x_range,
             libmerits.Increase(x_range).approach(1., alpha=alpha))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('approachmax.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('approachmax.pgf')

    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(0., 1.001, 0.001)
    alpha = 1
    lin_plot(x_range,
             libmerits.Decrease(x_range).tomin(alpha=alpha))
    alpha = 7
    pos_plot(x_range,
             libmerits.Decrease(x_range).tomin(alpha=alpha))
    alpha = 0.3
    neg_plot(x_range,
             libmerits.Decrease(x_range).tomin(lim_val=0.7, alpha=alpha))
    alpha = 1
    lin_plot(x_range,
             libmerits.Decrease(x_range).approach(0.7, alpha=alpha))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('approachmin.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('approachmin.pgf')


## RANGE (gaussian)
    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(-30., 30, 0.1)
    sigma = 15.
    pos_plot(x_range,
            libmerits.Range(x_range).coherence(sigma=sigma))
    sigma = 80.
    neg_plot(x_range,
            libmerits.Range(x_range).coherence(sigma=sigma))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('gaussian.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('gaussian.pgf')


## RANGE (smooth xboundary)
    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(-1., 1.001, .001)
    alpha = 0.1
    pos_plot(x_range,
            libmerits.Increase(x_range).xboundary(alpha=alpha))
    alpha = 0.2
    lin_plot(x_range,
            libmerits.Increase(x_range).xboundary(alpha=alpha))
    alpha = 0.4
    neg_plot(x_range,
            libmerits.Increase(x_range).xboundary(alpha=alpha))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('risexboundaryrangematrix.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('risexboundaryrangematrix.pgf')

    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(-1., 1.001, .001)
    alpha = 0.1
    pos_plot(x_range,
            libmerits.Decrease(x_range).xboundary(alpha=alpha))
    alpha = 0.2
    lin_plot(x_range,
            libmerits.Decrease(x_range).xboundary(alpha=alpha))
    alpha = 0.4
    neg_plot(x_range,
            libmerits.Decrease(x_range).xboundary(alpha=alpha))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('fallxboundaryrangematrix.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('fallxboundaryrangematrix.pgf')


## RANGE (inflection / window)
    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    x_range = np.arange(-1, 0.001, 0.001)
    alpha = 0.5
    pos_plot(x_range,
             libmerits.Range(x_range).inflection(alpha=alpha))
    alpha = 1.
    lin_plot(x_range,
             libmerits.Range(x_range).inflection(alpha=alpha))
    alpha = 3
    neg_plot(x_range,
             libmerits.Range(x_range).inflection(alpha=alpha))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('inflectionrangematrix.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('inflectionrangematrix.pgf')

    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    y_range = np.arange(0., 1.001, .001)
    y_range = np.hstack((y_range, np.arange(1., 0.001, -0.001)))
    y_range = 85.*np.hstack((np.zeros(100), y_range, np.zeros(100)))
    y_range = y_range[::5]
    pos_plot(np.arange(y_range.size),
             libmerits.Range(y_range).maximum())
    alpha = 1.
    lin_plot(np.arange(y_range.size),
             libmerits.Range(y_range).maximum(max_val=88., alpha=alpha))
    alpha = 2.
    neg_plot(np.arange(y_range.size),
             libmerits.Range(y_range).maximum(max_val=84., alpha=alpha))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('windowmaxmatrix.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('windowmaxmatrix.pgf')

    fig = plt.figure(figsize=figsize(0.2, aspect=1), facecolor='white', frameon=False)
    ax = plt.subplot(111, frameon=False)
    y_range = np.arange(0., -1.001, -.001)
    y_range = np.hstack((y_range, np.arange(-1., 0.001, 0.001)))
    y_range = 85.*np.hstack((np.zeros(100), y_range, np.zeros(100)))
    y_range = y_range[::5]
    pos_plot(np.arange(y_range.size),
             libmerits.Range(y_range).minimum())
    min_val = -7.
    lin_plot(np.arange(y_range.size),
             libmerits.Range(y_range).minimum(min_val=70.))
    plt.xticks(())
    plt.yticks(())
    plt.axis('tight')
    plt.savefig('windowminmatrix.png', facecolor=fig.get_facecolor(), transparent=True)
    plt.savefig('windowminmatrix.pgf')

    plt.show()
# -fin-
